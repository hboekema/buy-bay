""" The main program file """


import format_data
import csv_loader as loader
import analyse


def main():
    path = '../data/sql_data.csv'
    df = loader.load_csv(path)

    df = format_data.filter_by_price_range(df, max_range=100)
    analyse.price_change(df, id='product_id', slice=None, filter_properties=None, save_path='../data/stored_data/test.csv')


def run():
    path = '../data/sql_data.csv'
    df = loader.load_csv(path)

    df = format_data.filter_time_gaps(df, gap_size=30)
    df = format_data.min_price_every_day(df)
    analyse.price_change(df, id='product_id', save_path='../data/stored_data/min_prices.csv')


if __name__ == "__main__":
    run()
