""" This file contains useful data formatting functions """


import os
from datetime import datetime
import timeit
import pandas as pd
import matplotlib.pyplot as plt
import analyse
import csv_loader as loader


def filter_time_gaps(df, gap_size=30):
    """ Counts the number of times a gap of greater than or equal to gap_size in the days_ago column occurs. """
    temp_df = df.copy()

    # Ensure that days_ago was calculated
    try:
        temp_df['days_ago']
    except (IndexError, KeyError):
        temp_df = analyse.days_from_first(temp_df, id='product_id')

    # Sort by product_id, then by days_ago
    temp_df = temp_df.sort_values(by=['product_id', 'days_ago'], ascending=[True, True])
    temp_df['gap'] = temp_df['days_ago'].diff()

    # A cunning construction to overcome the difficulty of 'negative' gaps when there is a change in product_id
    temp_df[temp_df['gap'] < 0] = 0

    # Mark the product_ids that have large gaps
    blacklist = (temp_df[temp_df['gap'] > gap_size])['product_id'].tolist()

    i1 = df.set_index('product_id').index

    df = df[~i1.isin(blacklist)]

    return df.reset_index()



def min_price_every_day(df):
    """ Finds the minimum price of each product on every day. """
    if not isinstance(df.created_at.dtype, datetime):
        df['created_at'] = pd.to_datetime(df['created_at']).dt.date

    # Sort by the below categories, then drop to keep just the lowest price each day
    df = df.sort_values(by=['product_id', 'created_at', 'ref_new_price'], ascending=[True, True, True])
    return df.drop_duplicates(subset=['product_id', 'created_at', 'ref_new_price'], keep='first')


def save_price_time_data(path, cachedir, date_as_index=False, silent=False):
    """ Collects and saves price data in csv format """
    errors = 0  # Track any errors

    # Load and analyse data
    data = loader.load_csv(path)
    if not silent: print('Data collected.')
    result = analyse.price_over_time(data, merge_service_orderlines=True)
    if not silent: print('\nAnalysis performed.')

    # Convert dict of nested lists to dict of a DataFrame
    result_dataframe_dict = loader.list_dict_to_dataframe_dict(result, date_as_index=date_as_index)
    if not silent: print('Data conversion complete.')

    # Date to be used for the filenames
    today = datetime.now()
    today = today.date()

    # Separate out DataFrames and save these individually
    for product_id in result_dataframe_dict:
        try:
            # Create filename
            filename = 'POT ID' + str(product_id) + ' DATE' + str(today) + '.csv'

            # Save the file in the user-specified location
            result_dataframe_dict[product_id].to_csv(os.path.join(cachedir, filename))
        except:
            errors += 1

    print("Files saved in {} - {} errors.".format(cachedir, errors))


def format_product_data(path, cachedir=None, filename=None, silent=True):
    """ Formats data so that it is easily readable by the user """
    products = loader.load_csv(path)
    if not silent: print('\nData collected.')

    start = timeit.default_timer()

    avg_price = analyse.mean_price_by_product_id(products)
    if not silent: print("\nAverage price done.")
    min_price = analyse.min_price_by_product_id(products)
    if not silent: print("Minimum price found.")
    max_price = analyse.max_price_by_product_id(products)
    if not silent: print("Maximum price found.")
    price_changes = analyse.num_price_changes(products, diff_days=False)
    if not silent: print("Number of price changes calculated.")
    price_changes_diff_days = analyse.num_price_changes(products, diff_days=True)
    if not silent: print("Number of price changes (different days only) calculated.")
    price_change_over_time = analyse.price_change_over_time(products)
    if not silent: print("% price change between first and last price found.")
    time_range = analyse.range_available_time_data(products)
    if not silent: print("Time range done.")
    time_since_price_changed = analyse.since_product_price_change(products)
    if not silent: print("Time since last price change found.")
    data_density = analyse.data_time_density(products, data_num_price_changes=price_changes,
                                             data_range_available_time_data=time_range)
    if not silent: print("Data time density calculated.")

    errors = 0      # Tracks how many rows have been omitted
    product_dict = dict()

    product_ids = products['product_id']

    for product_id in product_ids:
        try:
            if product_id not in product_dict:
                # Shorthands are for simplicty (i.e. have no special meaning)
                avgp = round(avg_price[product_id], 2)
                minp = round(min_price[product_id], 2)
                maxp = round(max_price[product_id], 2)
                pran = round(maxp - minp, 2)    # The price range of this product
                prap = round((pran / avgp) * 100, 2)   # Price range as percentage of avg. price
                prch = price_changes[product_id]
                pcdd = price_changes_diff_days[product_id]
                pcot = round(price_change_over_time[product_id], 2)
                tmer = time_range[product_id]   # Range of time data for this product
                tspc = time_since_price_changed[product_id]
                dade = data_density[product_id]

                product_dict[product_id] = [avgp, minp, maxp, pran, prap, prch, pcdd, pcot, tmer, tspc, dade]
        except:
            errors += 1

    print('\nData creation done.')

    end = timeit.default_timer()
    time_taken = end - start

    print('\nAnalysis performed in {} seconds - {} errors (erroneous data is omitted).\n'.format(time_taken, errors))

    # Convert dict to a DataFrame for easy manipulation
    product_dataframe = pd.DataFrame.from_dict(data=product_dict, orient='index')
    product_dataframe.index.name = 'Product ID'
    product_dataframe.columns = ['Avg. Price', 'Min. Price', 'Max. Price', 'Price Range',
                                 'Price Range as % of Avg. Price', '# Price Changes', '# Price Changes (Diff. Days)',
                                 '% Price Change Over Time', 'Range of Time Data', 'Days Since Last Price Change',
                                 'Data Density']
    product_dataframe.sort_values('Range of Time Data', inplace=True)

    if cachedir is not None:    # Store file in specified directory
        now = datetime.now()
        if filename is None: filename = 'SOPC ' + str(now) + '.csv'     # Automatically generate a filename
        loader.save_to_csv(product_dataframe, dir=cachedir, filename=filename)     # Otherwise use user-specified name

    return product_dataframe


def plot_data(dataframe, product_id, date_as_index=True):
    data_dict = analyse.price_over_time(dataframe)
    dataframe_dict = loader.list_dict_to_dataframe_dict(data_dict, date_as_index)

    example = dataframe_dict[product_id]  # 110987
    print('{} data points.'.format(len(example)))

    if date_as_index: example.plot()
    else: example.plot(0)

    plt.show()


def add_data_to_csv(dataframe, load_path, save_path, filename='new_file.csv'):
    """ Adds new data to a CSV given a dataframe - WIP """
    other = loader.load_csv(load_path, format=False)

    # The product_ids are now indices in these dataframes
    df_ids = dataframe.index
    other_ids = other.index


def filter_by_price_range(df, max_range=30):
    """ Finds the range of prices of products in a DataFrame and filters out products with a range of more than
    'max_range' percent of the initial price. """
    # Find the range
    max = analyse.max_price_by_product_id(df, to_dict=False)
    min = analyse.min_price_by_product_id(df, to_dict=False)
    range_df = (max - min).reset_index()
    range_df.columns = ['product_id', 'range']

    # Find the first price of each product
    if not isinstance(df.created_at.dtype, datetime):
        df['created_at'] = pd.to_datetime(df['created_at'])

    no_duplicates = df.sort_values(by='created_at', ascending=True).drop_duplicates(subset='product_id', keep='first')\
        .reset_index()
    first_price = no_duplicates[['product_id'] + ['ref_new_price']].sort_values(by='product_id', ascending=True)

    # Perform the check and return all the entries that pass the condition
    range_df['range_cf_first_price'] = range_df['range'] / first_price['ref_new_price'] * 100
    range_df = range_df[range_df['range_cf_first_price'] <= max_range]

    i1 = df.set_index('product_id').index
    i2 = range_df.set_index('product_id').index

    return df[i1.isin(i2)]
