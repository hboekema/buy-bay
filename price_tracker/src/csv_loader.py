""" Loads data into python from a CSV file and produces an array of rows or Product objects """

import os
from dateutil import parser
import pandas as pd
import numpy as np
import product_data


def load_csv(path, format='raw'):
    """ Loads data from a CSV file into a pandas DataFrame with the option to format it appropriately (recommended) """
    if format is not None:
        if format == 'raw':  # Raw data
            dtypes = {'product_id': str, 'owner_products_id': str, 'service_orderline_id': str, 'old_price': float,
                      'new_price': float, 'ref_new_price': float, 'competitor': str, 'created_at': str,
                      'internal_category_id': str, 'sku_id': str}
            DF = pd.read_csv(path, dtype=dtypes)
            DF.columns = ['product_id', 'owner_products_id', 'service_orderline_id', 'old_price', 'new_price',
                              'ref_new_price', 'competitor', 'created_at', 'internal_category_id', 'sku_id']
        elif format == 'categorical':  # Category data
            DF = pd.read_csv(path, dtype=str)
            DF.columns = ['id', 'category_id', 'mapped_category_id', 'created_at', 'updated_at']
        elif format == 'processed':     # Processed data
            DF = pd.read_csv(path)
            DF.columns = ['Product ID', 'Avg. Price', 'Min. Price', 'Max. Price', 'Price Range',
                              'Price Range as % of Avg. Price', '# Price Changes', '# Price Changes (Diff. Days)',
                              'Range of Time Data', 'Days Since Last Price Change', 'Data Density']
            DF.set_index('Product ID', inplace=True)
    else:
        DF = pd.read_csv(path)
    return DF


def save_to_csv(dataframe, dir, filename='new_file.csv'):
    """ Saves DataFrame to a CSV """
    dataframe.to_csv(os.path.join(dir, filename))
    print('File \'{}\' saved to CSV with path {}'.format(filename, dir))


def str_to_dt(str):
    """ Converts str object to a datetime object """
    try:
        dt = parser.parse(str)
    except ValueError:
        return None

    return dt

def list_dict_to_dataframe_dict(list_dict, date_as_index=False):
    """ Converts a dict containing lists of lists (or tuples) into a dict containing dataframes """
    dataframe_dict = dict()

    for key in list_dict:
        if date_as_index:
            dataframe_dict[key] = pd.DataFrame(list_dict[key], columns=['Date', 'Price']).set_index('Date')
            dataframe_dict[key].sort_index(inplace=True)
        else:
            dataframe_dict[key] = pd.DataFrame(list_dict[key], columns=['Date', 'Price'])
            dataframe_dict[key].sort_values('Date', inplace=True)

    return dataframe_dict


""" Not really used """


def dataframe_to_series(dataframe):
    """ Returns a pandas Series given a pandas DataFrame """
    return dataframe.values


def rows_to_objects(rows):
    """ Returns an array of Product objects given the rows of an array """
    if type(rows) is pd.DataFrame: rows = dataframe_to_series(rows)
    temp = []

    # Creates a list of Products using the rows of the array/Series
    for row in rows:
        temp.append(product_data.Product(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7]))

    return np.array(temp)


def objects_to_rows(products):
    """ Converts an array of Product objects to rows in an array """
    if type(products) is pd.DataFrame: products = dataframe_to_series(products)
    temp = []

    # Creates a list of rows with data from the Product objects
    for product in products:
        temp.append([product.product_id, product.owner_products_id, product.service_orderline_id, product.old_price,
                     product.new_price, product.competitor, product.ref_new_price, product.created_at])

    return np.array(temp)


def dict_fillna(dictionary):
    """ Finds the maximum entry size in a dictionary and fills the other entries with None to fit this size """
    if type(dictionary) is not dict:
        print('Argument is not of type dict')
        return dict

    max_length = 0

    for key in dictionary:
        if len(dictionary[key]) > max_length:
            max_length = len(dictionary[key])

    for key in dictionary:
        for i in range(max_length - len(dictionary[key])):
            # Append None until this dictionary value is of the same length as max_length
            dictionary[key].append(None)

    return dictionary
