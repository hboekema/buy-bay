""" This file contains the functions that are used to analyse datasets and extract useful infoormation from these """


from datetime import datetime
import pandas as pd
import numpy as np
import csv_loader as loader
import product_data


""" Dictionaries with product_id keys that use pandas DataFrame functions """

def mean_price_by_product_id(dataframe, to_dict=True):
    sorted_dataframe = dataframe.groupby('product_id').mean()
    if to_dict: return sorted_dataframe['ref_new_price'].to_dict()
    else: return sorted_dataframe['ref_new_price']


def max_price_by_product_id(dataframe, to_dict=True):
    sorted_dataframe = dataframe.groupby('product_id').max()
    if to_dict: return sorted_dataframe['ref_new_price'].to_dict()
    else: return sorted_dataframe['ref_new_price']


def min_price_by_product_id(dataframe, to_dict=True):
    sorted_dataframe = dataframe.groupby('product_id').min()
    if to_dict: return sorted_dataframe['ref_new_price'].to_dict()
    else: return sorted_dataframe['ref_new_price']


def first_date(dataframe, to_dict=True):
    dataframe['created_at'] = pd.to_datetime(dataframe['created_at'])
    sorted_dataframe = dataframe.groupby('product_id').min()
    if to_dict: return sorted_dataframe['created_at'].to_dict()
    else: return sorted_dataframe['created_at']


def last_date(dataframe, to_dict=True):
    dataframe['created_at'] = pd.to_datetime(dataframe['created_at'])
    sorted_dataframe = dataframe.groupby('product_id').max()
    if to_dict: return sorted_dataframe['created_at'].to_dict()
    else: return sorted_dataframe['created_at']


""" Dictionaries with product_id keys that do not use pandas DataFrame functions """


def range_available_time_data(dataframe):
    """ Finds the range of time data for each product_id. """
    first = first_date(dataframe)
    last = last_date(dataframe)
    range = dict()

    product_ids = dataframe['product_id']

    for product_id in product_ids:
        try:
            # Calculate the time, in days, between the first data point and the last data point 
            range[product_id] = (loader.str_to_dt(last[product_id]).date() -
                                 loader.str_to_dt(first[product_id]).date()).days
        except ValueError:
            # Ignore this data point 
            range[product_id] = None

    return range


def num_price_changes(dataframe, diff_days=True):
    """ Returns the number of price changes that have occurred since the creation of this product """

    change_dict = dict()

    if diff_days is False:
        product_ids = dataframe['product_id']
        for product_id in product_ids:
            if product_id not in change_dict:
                change_dict[product_id] = 0

            change_dict[product_id] += 1    # Increment this every time a product is found

    else:
        length = len(dataframe)
        days_dict = dict()

        cols = ['product_id'] + ['created_at']
        df = dataframe[cols]

        for i in range(length):
            # Prepare for the test conditions (below)
            row = df.ix[i]
            product_id = row['product_id']
            created = loader.str_to_dt(row['created_at'])

            if product_id not in days_dict:
                days_dict[product_id] = []

            if product_id not in change_dict:
                change_dict[product_id] = 0

            # The following code determines whether or not the prices for this product should be included
            skip = False

            day_created = created.date()
            if day_created in days_dict[product_id]:
                skip = True

            if skip is False:
                days_dict[product_id].append(day_created)   # Add this date to the days_dict
                change_dict[product_id] += 1

    return change_dict


def data_time_density(dataframe, data_num_price_changes=None, data_range_available_time_data=None):
    """ Calculates the density of data points for each product """
    density_dict = dict()

    # The following allows the user to provide the required data as arguments to this function and so avoid unnecessary
    # (cumbersome) calculations
    if data_num_price_changes is None:
        price_changes = num_price_changes(dataframe)
    else:
        price_changes = data_num_price_changes

    if data_range_available_time_data is None:
        time = range_available_time_data(dataframe)
    else:
        time = data_range_available_time_data

    product_ids = dataframe['product_id']

    for product_id in product_ids:
        try:
            # Evaluate the data density, avoiding errors
            density_dict[product_id] = round(price_changes[product_id] / time[product_id], 2)
        except ZeroDivisionError:
            density_dict[product_id] = None

    return density_dict


def price_over_time(dataframe, merge_service_orderlines=True):
    """ Finds and stores the price of a product over time """
    price_dict = dict()

    if merge_service_orderlines:
        cols = ['product_id'] + ['ref_new_price'] + ['created_at']  # Only consider the useful data
    else:
        cols = ['product_id'] + ['service_orderline_id'] + ['ref_new_price'] + ['created_at']

    df = dataframe[cols]
    length = len(df.index)

    for i in range(length):
        # Get the product_id, ref_new_price and created_at data out of this row
        row = df.ix[i]
        product_id = row['product_id']
        created = row['created_at']
        ref_price = row['ref_new_price']

        if merge_service_orderlines:
            # Ignore service orderlines and only consider the product_id as a discriminator
            if product_id not in price_dict:
                price_dict[product_id] = []

            price_dict[product_id].append((created, ref_price))  # Add a (date, price) tuple to
            # list

        else:
            # Create a dictionary containing a dictionary of tuples
            orderline_id = row['service_orderline_id']
            if product_id not in price_dict:
                price_dict[product_id] = dict()

            # In this case, the second dictionary is needed to keep track of price changes for each individual
            # product rather than the product in general
            if orderline_id not in price_dict[product_id]:
                (price_dict[product_id])[orderline_id] = []

            (price_dict[product_id])[orderline_id].append((created, ref_price))

    return price_dict


def price_change_over_time(dataframe, price_data=None):
    """ Returns the change in price (as a percentage of the original price) between the first and last price in the
    DataFrame. The parameter price_data, if provided, allows the program to skip the calculation of prices over
    time. """
    if price_data is None: price_data = price_over_time(dataframe)    # Significantly extends runtime

    price_change = dict()

    product_ids = dataframe['product_id']

    for product_id in product_ids:
        # Just get first and last price entry from the price_data (see price_over_time() for more info)
        first = price_data[product_id][0][1]    # This retrieves the second entry (price) of the first entry (first data
        #  point) of the value associated with product_id
        last = price_data[product_id][-1][1]    # This retrieves the second entry (price) of the last entry (last data
        #  point) of the value associated with product_id

        price_change[product_id] = 100 * (last - first) / first

    return price_change


def since_product_price_change(dataframe):
    """ Returns the last time the product prices were updated (in days) """
    time_dict = dict()

    cols = ['product_id'] + ['created_at']
    df = dataframe[cols]
    length = len(df)

    for i in range(length):
        row = df.ix[i]
        product_id = row['product_id']
        product_creation = time_since_creation(row)

        if product_id not in time_dict:
            time_dict[product_id] = product_creation

        else:
            # Only consider the most recent price change
            most_recent = time_dict[product_id]

            if product_creation < most_recent:
                time_dict[product_id] = product_creation

    return time_dict


""" Dictionaries with service_orderline_id keys """


def since_orderline_price_change(products):
    """ Returns the last time the orderline prices were updated (in days) """
    if type(products) is pd.DataFrame: products = loader.dataframe_to_series(products)
    if type(products[0]) is not product_data.Product: products = loader.rows_to_objects(products)

    time_dict = dict()

    for product in products:
        orderline_id = product.service_orderline_id
        time_dict[orderline_id] = product.time_since_creation(days_only=True)

    return time_dict


def last_price_change(products):
    """ Returns the value of the last price change """
    if type(products) is pd.DataFrame: products = loader.dataframe_to_series(products)
    if type(products[0]) is not product_data.Product: products = loader.rows_to_objects(products)

    time_dict = dict()      # Will be used to ensure that only the last price change is considered
    price_change_dict = dict()

    for product in products:
        if product.created_at is not None:    # Omit non-existent data
            orderline_id = product.service_orderline_id

            if orderline_id not in time_dict:
                time_dict[orderline_id] = product.created_at
                try:
                    price_change_dict[orderline_id] = round(product.new_price - product.old_price, 2)
                except ValueError:
                    price_change_dict[orderline_id] = None

            elif product.created > time_dict[orderline_id]:    # This limits the dict to only store the most recent
                # change
                try:
                    price_change_dict[orderline_id] = round(product.new_price - product.old_price, 2)
                except ValueError:
                    price_change_dict[orderline_id] = None

    return price_change_dict


""" Miscellaneous functions """


def time_since_creation(datum, days_only=True):
    """ Finds the time a certain product has spent on the market - argument datum should be a row of a DataFrame """
    now = datetime.now()
    try:
        created_string = datum['created_at']
        created = loader.str_to_dt(created_string)
        timedelta = now - created
    except ValueError:
        return None

    if days_only:
        return timedelta.days
    else:
        return timedelta


def time_on_market(df, days_only=True):
    """ Finds time a product has spent on market """
    first = first_date(df, to_dict=False)
    last = last_date(df, to_dict=False)

    time = last - first
    if days_only:
        time = time.days

    return time


def days_from_first(df, id='product_id'):
    # Adds a column containing the number of days since the first entry of a product to the DataFrame
    if not isinstance(df.created_at.dtype, datetime):
        df['created_at'] = pd.to_datetime(df['created_at'])

    no_duplicates = df.sort_values(by='created_at', ascending=True).drop_duplicates(subset=id, keep='first')

    first_price = no_duplicates[[id] + ['created_at']]
    first_price.columns = [id, 'first_date']
    merged_df = pd.merge(df, first_price, on=id, how='left')

    df['days_ago'] = merged_df['created_at'] - merged_df['first_date']
    df['days_ago'] = df['days_ago'].dt.days

    df.dropna(subset=['days_ago'])
    df = df.drop_duplicates(subset=[id, 'ref_new_price', 'days_ago'])

    df = df.sort_values(by=[id, 'days_ago'])

    return df


def price_change(df, id='product_id', slice=None, filter_properties=None, save_path=None):
    """ Tracks the price changes of products as a percentage of the initial price. Groups by days. """
    # Prepare DataFrame
    df['created_at'] = pd.to_datetime(df['created_at'])
    df['created_at'] = df['created_at'].dt.date

    # Sort DataFrame by date created and then drop duplicate products, keeping just the first entry
    df = df.drop_duplicates(subset=[id, 'created_at'], keep='first')
    df = df.dropna(how='any', subset=['created_at', 'ref_new_price']).reset_index()

    # Apply filters
    try:
        filter = filter_properties[0]
    except (IndexError, TypeError):
        filter = None

    if filter is not None:
        if filter == 'date':
            df = price_change_filters(df, filter_properties, id)

            if slice is not None:
                df = df[:slice]

            if save_path is not None:
                # Save the file in the user-specified location
                df.to_csv(save_path)
            return df

        elif filter == 'category' and id != 'product_id':
            print('This filter needs a product id to work. Current id: {}'.format(id))
            df = df
        else:
            df = price_change_filters(df, filter_properties, id)

    df = days_from_first(df, id)
    no_duplicates = df.sort_values(by='days_ago', ascending=True).drop_duplicates(subset=id, keep='first')

    first_price = no_duplicates[[id] + ['ref_new_price']]
    first_price.columns = [id, 'first_price']

    # Merge DataFrames and divide each price by the first price for each product
    merged_df = pd.merge(df, first_price, on=id, how='inner')
    merged_df['% original price'] = merged_df['ref_new_price'] / merged_df['first_price'] * 100

    # Now pivot the table such that the mean % price of each product is shown
    df_table = pd.pivot_table(merged_df, index=['days_ago'], values=['% original price'], aggfunc=('count', 'mean'))

    if slice is not None:
        df_table = df_table[:slice]

    if save_path is not None:
        # Save the file in the user-specified location
        df_table.to_csv(save_path)

    return df_table


def price_change_filters(df, filter_properties, id):
    """ Contains the callable filters from price_change """
    filter = filter_properties[0]

    if filter == 'price':
        try:
            threshold = filter_properties[1]
        except IndexError as e:
            print('\n{} raised. Default used.'.format(e))
            threshold = 0

        # Remove products with fewer than a certain number of price entries
        price_table = pd.pivot_table(df, index=[id], values=['ref_new_price'], aggfunc=len)
        price_table = price_table.unstack().reset_index().drop('level_0', axis=1)
        price_table = price_table[price_table[0] >= threshold]

        # Only keep products in df that are also in (the filtered) price_table
        i1 = df.set_index(id).index
        i2 = price_table.set_index(id).index

        df = df[i1.isin(i2)]

    elif filter == 'date':
        # Filter products that do not have a price on this date
        try:
            date = datetime.strptime(filter_properties[1], '%d/%m/%Y').date()
        except IndexError as e:
            print('\n{} raised. Default used.'.format(e))
            date = datetime.strptime('01/01/2017', '%d/%m/%Y').date()

        filtered_values = df[df['created_at'] == date]

        i1 = df.set_index(id).index
        i2 = filtered_values.set_index(id).index

        df = df[i1.isin(i2)]

        # Now only display price entries that have been entered into the database since 'date'
        df['days since start'] = df['created_at'] - date
        df = df[df['days since start'].dt.days >= 0]

        no_duplicates = df.sort_values(by='days since start', ascending=True).drop_duplicates(subset=id,
                                                                                          keep='first')

        first_price = no_duplicates[[id] + ['ref_new_price']]
        first_price.columns = [id, 'first_price']

        # Merge DataFrames and divide each price by the first price for each product
        merged_df = pd.merge(df, first_price, on=id, how='inner')
        merged_df['% original price'] = merged_df['ref_new_price'] / merged_df['first_price'] * 100

        # Now pivot the table such that the mean % price of each product is shown
        df_table = pd.pivot_table(merged_df, index=['days since start'], values=['% original price'],
                                    aggfunc=('mean', 'count'))

        return df_table

    elif filter == 'category':
        # Only consider the x most prominent categories
        try:
            x = filter_properties[1]
        except IndexError as e:
            print('\n{} raised. Default used.'.format(e))
            x = 0

        # Load the categories and join these to the DataFrame
        categories_df = loader.load_csv('../data/category_mappings.csv')

        categories_df = pd.DataFrame(
            categories_df.groupby(['category_id'])['mapped_category_id'].apply(list)).reset_index()

        # Ensures that the merge (below) works
        df['internal_category_id'] = df['internal_category_id'].astype(str)
        categories_df['category_id'] = categories_df['category_id'].astype(str)

        df = df.merge(categories_df, how='left', left_on='internal_category_id', right_on='category_id')

        # Speed up the below operation
        df = df.dropna(subset=['mapped_category_id'])
        temp = df.drop_duplicates(subset=['product_id'])
        temp = temp[['product_id', 'mapped_category_id']]

        # Explode the lists in 'mapped_category_id' into column values
        cat_ids = temp['mapped_category_id'].apply(pd.Series)
        temp = pd.concat([temp, cat_ids], axis=1)
        temp = temp.drop(labels=['mapped_category_id'], axis='columns')

        # Now melt DataFrame to convert the columns created above to row entries
        temp = pd.melt(temp, id_vars=['product_id'], var_name='space_filler', value_name='cat_id')
        temp = temp.drop(labels=['space_filler'], axis='columns')

        # Obtain the top x categories
        cats = pd.pivot_table(temp, index=['cat_id'], aggfunc='count')
        cats = cats.sort_values(by='product_id', ascending=False)
        cats = cats.unstack().reset_index().drop('level_0', axis=1)
        use_categories = cats[:x]

        # Find product ids of products that fall in the top x categories
        ii1 = temp.set_index('cat_id').index
        ii2 = use_categories.set_index('cat_id').index

        use_ids = temp[ii1.isin(ii2)]

        # Only keep products in df that are also in use_ids
        i1 = df.set_index('product_id').index
        i2 = use_ids.set_index('product_id').index

        df = df[i1.isin(i2)]

    df = df.reset_index()
    return df
