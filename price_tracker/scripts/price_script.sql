USE buybay;
SELECT 
products.id,
owner_products.id,
service_orderlines.id,
service_orderline_price_changes.old_price,
service_orderline_price_changes.new_price,
service_orderline_price_changes.ref_new_price,
service_orderline_price_changes.competitor,
service_orderline_price_changes.created_at,
products.internal_category_id,
service_orderlines.sku_id
FROM service_orderline_price_changes
INNER JOIN service_orderlines ON service_orderlines.id = service_orderline_price_changes.service_orderline_id
INNER JOIN owner_products ON owner_products.id = service_orderlines.owner_product_id
INNER JOIN products ON products.id = owner_products.product_id
WHERE service_orderline_price_changes.ref_new_price IS NOT NULL
AND service_orderline_price_changes.competitor NOT LIKE '%stock%'
INTO OUTFILE 'sql_data.csv'
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
ESCAPED BY '\\' 
LINES TERMINATED BY '\n'
