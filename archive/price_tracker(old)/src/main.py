""" The main program file """

import format_data
import matplotlib.pyplot as plt
import csv_loader as loader
import analyse


def main():
    path = '../data/sql_data.csv'
    format_data.format_product_data(path, cachedir= '../data/stored_data',silent=False)
    #format_data.save_price_time_data(path, cachedir='../data/stored_data/price_over_time')

    """

    print('Data density: {}'.format(analyse.data_time_density(data)))
    print('Range of time data (days): {}'.format(analyse.range_available_time_data(data)))
    print('Number of price changes: {}'.format(analyse.num_price_changes(data))) """

    """
    data = loader.load_csv(path)

    # Fun id's to plot:
    # 179151
    # 26180
    format_data.plot_data(data, 26180)
    """

if __name__ == "__main__":
    main()
