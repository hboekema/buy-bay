""" This file contains the functions that are used to analyse datasets and extract useful infoormation from these """


import pandas as pd
import csv_loader as loader
import product_data


""" Dictionaries with product_id keys that use pandas DataFrame functions """

def mean_price_by_product_id(dataframe):
    sorted_dataframe = dataframe.groupby('product_id').mean()
    return sorted_dataframe['ref_new_price'].to_dict()


def max_price_by_product_id(dataframe):
    sorted_dataframe = dataframe.groupby('product_id').max()
    return sorted_dataframe['ref_new_price'].to_dict()


def min_price_by_product_id(dataframe):
    sorted_dataframe = dataframe.groupby('product_id').min()
    return sorted_dataframe['ref_new_price'].to_dict()


def first_date(dataframe):
    sorted_dataframe = dataframe.groupby('product_id').min()
    return sorted_dataframe['created_at'].to_dict()


def last_date(dataframe):
    sorted_dataframe = dataframe.groupby('product_id').max()
    return sorted_dataframe['created_at'].to_dict()


""" Dictionaries with product_id keys that do not use pandas DataFrame functions """


def range_available_time_data(dataframe, silent=True):
    """ Finds the range of time data for each product_id """
    first = first_date(dataframe)
    last = last_date(dataframe)
    range = dict()

    products = loader.dataframe_to_series(dataframe)
    if type(products[0]) is not product_data.Product: products = loader.rows_to_objects(products)

    length = len(products)
    count = 0

    for product in products:
        product_id = product.product_id

        try:
            # Calculate the time, in days, between the first data point and the last data point
            range[product_id] = (loader.str_to_dt(last[product_id]).date() -
                                 loader.str_to_dt(first[product_id]).date()).days

        except ValueError:
            # Ignore this data point
            range[product_id] = None

        if not silent:
            count += 1
            print("{} / {} done".format(count, length))

    return range


def num_price_changes(products, diff_days=True, silent=True):
    """ Returns the number of price changes that have occurred since the creation of this product """
    if type(products) is pd.DataFrame: products = loader.dataframe_to_series(products)
    if type(products[0]) is not product_data.Product: products = loader.rows_to_objects(products)

    change_dict = dict()

    length = len(products)
    count = 0

    if diff_days is False:
        for product in products:
            product_id = product.product_id

            if product_id not in change_dict:
                change_dict[product_id] = 0

            if not silent:
                count += 1
                print("{} / {} done".format(count, length))

            change_dict[product_id] += 1    # Increment this every time a product is found

    else:
        days_dict = dict()

        for product in products:
            # Prepare for the test conditions (below)
            product_id = product.product_id
            created = product.created_at

            if product_id not in days_dict:
                days_dict[product_id] = []

            if product_id not in change_dict:
                change_dict[product_id] = 0

            # The following code determines whether or not the prices for this product should be included
            skip = False
            for date in days_dict[product_id]:
                if created.date() == date.date():
                    skip = True

            if skip is False:
                days_dict[product_id].append(created)   # Add this date to the days_dict
                change_dict[product_id] += 1

    return change_dict


def data_time_density(products, data_num_price_changes=None, data_range_available_time_data=None):
    """ Calculates the density of data points for each product """
    density_dict = dict()

    # The following allows the user to provide the required data as arguments to this function and so avoid unnecessary
    # (cumbersome) calculations
    if data_num_price_changes is None:
        price_changes = num_price_changes(products)
    else:
        price_changes = data_num_price_changes

    if data_range_available_time_data is None:
        time = range_available_time_data(products)
    else:
        time = data_range_available_time_data

    # Convert 'products' to the right type
    if type(products) is pd.DataFrame: products = loader.dataframe_to_series(products)
    if type(products[0]) is not product_data.Product: products = loader.rows_to_objects(products)

    for product in products:
        product_id = product.product_id

        try:
            # Evaluate the data density, avoiding errors
            density_dict[product_id] = round(price_changes[product_id] / time[product_id], 2)
        except ZeroDivisionError:
            density_dict[product_id] = None

    return density_dict


def price_over_time(products, merge_service_orderlines=True):
    """ Finds and stores the price of a product over time """
    if type(products) is pd.DataFrame: products = loader.dataframe_to_series(products)
    if type(products[0]) is not product_data.Product: products = loader.rows_to_objects(products)

    price_dict = dict()

    for product in products:
        product_id = product.product_id

        if merge_service_orderlines:
            if product_id not in price_dict:
                price_dict[product_id] = []

            price_dict[product_id].append((product.created_at, product.ref_new_price))  # Add a (date, price) tuple to
            # list

        else:
            # Create a dictionary containing a dictionary of tuples
            if product_id not in price_dict:
                price_dict[product_id] = dict()

            orderline_id = product.service_orderline_id

            # In this case, the second dictionary is needed to keep track of price changes for each individual
            # (physical) product rather than the product in general
            if orderline_id not in price_dict[product_id]:
                (price_dict[product_id])[orderline_id] = []

            (price_dict[product_id])[orderline_id].append((product.created_at, product.ref_new_price))

    return price_dict


def since_product_price_change(products):
    """ Returns the last time the product prices were updated (in days) """
    if type(products) is pd.DataFrame: products = loader.dataframe_to_series(products)
    if type(products[0]) is not product_data.Product: products = loader.rows_to_objects(products)

    time_dict = dict()

    for product in products:
        product_id = product.product_id
        product_creation = product.time_since_creation()

        if product_id not in time_dict:
            time_dict[product_id] = product_creation

        else:
            # Only consider the most recent price change
            most_recent = time_dict[product_id]

            if product_creation < most_recent:
                time_dict[product_id] = product_creation

    return time_dict


""" Dictionaries with service_orderline_id keys """


def since_orderline_price_change(products):
    """ Returns the last time the orderline prices were updated (in days) """
    if type(products) is pd.DataFrame: products = loader.dataframe_to_series(products)
    if type(products[0]) is not product_data.Product: products = loader.rows_to_objects(products)

    time_dict = dict()

    for product in products:
        orderline_id = product.service_orderline_id
        time_dict[orderline_id] = product.time_since_creation(days_only=True)

    return time_dict


def last_price_change(products):
    """ Returns the value of the last price change """
    if type(products) is pd.DataFrame: products = loader.dataframe_to_series(products)
    if type(products[0]) is not product_data.Product: products = loader.rows_to_objects(products)

    time_dict = dict()      # Will be used to ensure that only the last price change is considered
    price_change_dict = dict()

    for product in products:
        if product.created_at is not None:    # Omit non-existent data
            orderline_id = product.service_orderline_id

            if orderline_id not in time_dict:
                time_dict[orderline_id] = product.created_at
                try:
                    price_change_dict[orderline_id] = round(product.new_price - product.old_price, 2)
                except ValueError:
                    price_change_dict[orderline_id] = None

            elif product.created > time_dict[orderline_id]:    # This limits the dict to only store the most recent
                # change
                try:
                    price_change_dict[orderline_id] = round(product.new_price - product.old_price, 2)
                except ValueError:
                    price_change_dict[orderline_id] = None

    return price_change_dict
