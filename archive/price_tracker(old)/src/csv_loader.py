""" Loads data into python from a CSV file and produces an array of rows or Product objects """

from dateutil import parser
import pandas as pd
import numpy as np
import product_data


def load_csv(path, format=True):
    """ Loads data from a CSV file into a pandas DataFrame with the option to format it appropriately (recommended) """
    if format is False: return pd.read_csv(path)
    else:
        DF = pd.read_csv(path)
        DF.columns = ['product_id', 'owner_products_id', 'service_orderline_id', 'old_price', 'new_price',
                      'ref_new_price', 'competitor', 'created_at']
        return DF


def dataframe_to_series(dataframe):
    """ Returns a pandas Series given a pandas DataFrame """
    return dataframe.values


def rows_to_objects(rows):
    """ Returns an array of Product objects given the rows of an array """
    if type(rows) is pd.DataFrame: rows = dataframe_to_series(rows)
    temp = []

    # Creates a list of Products using the rows of the array/Series
    for row in rows:
        temp.append(product_data.Product(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7]))

    return np.array(temp)


def objects_to_rows(products):
    """ Converts an array of Product objects to rows in an array """
    if type(products) is pd.DataFrame: products = dataframe_to_series(products)
    temp = []

    # Creates a list of rows with data from the Product objects
    for product in products:
        temp.append([product.product_id, product.owner_products_id, product.service_orderline_id, product.old_price,
                     product.new_price, product.competitor, product.ref_new_price, product.created_at])

    return np.array(temp)


def list_dict_to_dataframe_dict(list_dict, date_as_index=False):
    """ Converts a dict containing lists of lists (or tuples) into a dict containing dataframes """
    dataframe_dict = dict()

    for key in list_dict:
        if date_as_index:
            dataframe_dict[key] = pd.DataFrame(list_dict[key], columns=['Date', 'Price']).set_index('Date')
            dataframe_dict[key].sort_index(inplace=True)
        else:
            dataframe_dict[key] = pd.DataFrame(list_dict[key], columns=['Date', 'Price'])
            dataframe_dict[key].sort_values('Date', inplace=True)

    return dataframe_dict


def dataframe_dict_to_csv(dataframe_dict):
    """ Converts dict of dataframes to a csv file """
    pass


def str_to_dt(str):
    """ Converts str object to a datetime object """
    try:
        dt = parser.parse(str)
    except ValueError:
        return None

    return dt


""" Not really used """

def dict_fillna(dictionary):
    """ Finds the maximum entry size in a dictionary and fills the other entries with None to fit this size """
    if type(dictionary) is not dict:
        print('Argument is not of type dict')
        return dict

    max_length = 0

    for key in dictionary:
        if len(dictionary[key]) > max_length:
            max_length = len(dictionary[key])

    for key in dictionary:
        for i in range(max_length - len(dictionary[key])):
            # Append None until this dictionary value is of the same length as max_length
            dictionary[key].append(None)

    return dictionary
