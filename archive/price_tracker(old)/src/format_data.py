""" This file contains useful data formatting functions """


import os
from datetime import datetime
import timeit
import pandas as pd
import matplotlib.pyplot as plt
import analyse
import csv_loader as loader


def save_price_time_data(path, cachedir, date_as_index=False):
    """ Collects and saves price data in csv format """
    errors = 0  # Track any errors

    # Load and analyse data
    data = loader.load_csv(path)
    result = analyse.price_over_time(data, merge_service_orderlines=True)

    # Convert dict of nested lists to dict of a dataframe
    result_dataframe_dict = loader.list_dict_to_dataframe_dict(result, date_as_index=date_as_index)

    # Date to be used for the filenames
    today = datetime.now()
    today = today.date()

    # Separate out DataFrames and save these individually
    for product_id in result_dataframe_dict:
        try:
            # Create filename
            filename = 'POT ID' + str(product_id) + ' DATE' + str(today) + '.csv'

            # Save the file in the user-specified location
            result_dataframe_dict[product_id].to_csv(os.path.join(cachedir, filename))

        except:
            errors += 1

    print("Files saved in {} - {} errors.".format(cachedir, errors))


def format_product_data(path, cachedir=None, silent=True):
    """ Formats data so that it is easily readable by the user """
    products = loader.load_csv(path)

    start = timeit.default_timer()

    avg_price = analyse.mean_price_by_product_id(products)
    if not silent: print("\nAverage price done.")
    min_price = analyse.min_price_by_product_id(products)
    if not silent: print("Minimum price found.")
    max_price = analyse.max_price_by_product_id(products)
    if not silent: print("Maximum price found.")
    price_changes = analyse.num_price_changes(products, diff_days=False, silent=silent)
    if not silent: print("Number of price changes calculated.")
    price_changes_diff_days = analyse.num_price_changes(products, diff_days=True)
    if not silent: print("Number of price changes (different days only) calculated.")
    time_range = analyse.range_available_time_data(products)
    if not silent: print("Time range done.")
    time_since_price_changed = analyse.since_product_price_change(products)
    if not silent: print("Time since last price change found.")
    data_density = analyse.data_time_density(products, data_num_price_changes=price_changes,
                                             data_range_available_time_data=time_range)
    if not silent: print("Data time density calculated.")

    errors = 0      # Tracks how many rows have been omitted
    product_dict = dict()
    products = loader.dataframe_to_series(products)     # Convert so that the product_id can be accessed easily

    for product in products:
        product_id = product[0]     # This gives the product_id without the need to convert to Product objects

        try:
            if product_id not in product_dict:
                # Shorthands are for simplicty (i.e. have no special meaning)
                avgp = round(avg_price[product_id], 2)
                minp = round(min_price[product_id], 2)
                maxp = round(max_price[product_id], 2)
                pran = round(maxp - minp, 2)    # The price range of this product
                prap = round((pran / avgp) * 100, 2)   # Price range as percentage of avg. price
                prch = price_changes[product_id]
                pcdd = price_changes_diff_days[product_id]
                tmer = time_range[product_id]   # Range of time data for this product
                tspc = time_since_price_changed[product_id]
                dade = data_density[product_id]

                product_dict[product_id] = [avgp, minp, maxp, pran, prap, prch, pcdd, tmer, tspc, dade]
        except:
            errors += 1

    end = timeit.default_timer()
    time_taken = end - start

    print('\nAnalysis performed in {} seconds - {} errors (erroneous data is omitted).\n'.format(time_taken, errors))

    # Convert dict to a DataFrame for easy manipulation
    product_dataframe = pd.DataFrame.from_dict(data=product_dict, orient='index')
    product_dataframe.columns = ['Avg. Price', 'Min. Price', 'Max. Price', 'Price Range',
                                 'Price Range as % of Avg. Price', '# Price Changes', '# Price Changes (Diff. Days)',
                                 'Range of Time Data', 'Days Since Last Price Change', 'Data Density']
    product_dataframe.sort_values('Range of Time Data', inplace=True)

    if cachedir is not None:    # Store file in specified directory
        now = datetime.now()
        filename = 'SOPC ' + str(now) + '.csv'
        product_dataframe.to_csv(os.path.join(cachedir, filename))
        print('File stored with path {}.'.format(cachedir))

    return product_dataframe


def plot_data(dataframe, product_id, date_as_index=True):
    data_dict = analyse.price_over_time(dataframe)
    dataframe_dict = loader.list_dict_to_dataframe_dict(data_dict, date_as_index)

    example = dataframe_dict[product_id]  # 110987
    print('{} data points.'.format(len(example)))

    if date_as_index: example.plot()
    else: example.plot(0)

    plt.show()