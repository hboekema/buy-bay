""" This file contains a class representing a product. This class allows for easy manipulation of product properties """

from dateutil import parser
from datetime import datetime


class Product():
    def __init__(self, product_id, owner_products_id, service_orderline_id, old_price, new_price, ref_new_price,
                 competitor, created_at):
        self.product_id = product_id
        self.owner_products_id = owner_products_id
        self.service_orderline_id = service_orderline_id
        self.old_price = old_price
        self.new_price = new_price
        self.ref_new_price = ref_new_price
        self.competitor = competitor

        try:
            self.created_at = parser.parse(created_at)
        except ValueError:
            self.created_at = None

    def time_since_creation(self, days_only=True):
        """ Finds the time a certain product has spent on the market """
        now = datetime.now()
        try:
            timedelta = now - self.created_at
        except ValueError:
            return None

        if days_only:
            return timedelta.days
        else:
            return timedelta

