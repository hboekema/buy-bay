This archive contains deprecated/unused functions and files for src.model:

format.txt: contains functions related to formatting and feature creation that have proven not to be useful.

evaluate.txt: unnecessary and inefficient functions that were at one point useful in evaluating the model.

evluate.py: useful functions for evaluating the model. Have not been included in the final model files because they are not needed. As such, they have been archived such that future data scientists may continue to work on and improve the MAchine Learning model.

model.txt: contains a linear regression model that was experimented with. Howeber, it was found that the time taken to train models was excessive compared to XGBoost.


