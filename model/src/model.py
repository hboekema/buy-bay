""" This file contains the machine learning and statistical models that will be used to predict product prices over a
time period. """


import pickle
import time
import datetime

import xgboost as xgb
import pandas as pd
import numpy as np

import format
import csv_loader as loader


""" Functions that generate or act as interfaces for generating XGBoost mappings or models """


def generate_model(data=None, train_names=None, test_names=None, load_dir=None, save_dir=None):
    """ Generates the XGBoost regression model. Param 'data' is a list of data to be used, otherwise param 'load_dir'
    should be set. Param 'load_dir' (if used) should be a list of (5) load directories. 'save_dir' is the path to the
    directory in which the model will be saved. """

    # Check if user is being sensible
    if data is None and load_dir is None:
        print('\nNo data given. Exiting program.')
        return 1

    elif data is not None and load_dir is not None:
        print('\nParams \'data\' and \'load_dir\' cannot both be used. Using default (\'data\').')
        load_dir = None

    if load_dir is not None:
        # Use load directories
        train_X_loaded = loader.load_sparse_csr(load_dir[0])
        train_y_loaded = np.load(load_dir[1])
        test_X_loaded = loader.load_sparse_csr(load_dir[2])
        test_y_loaded = np.load(load_dir[3])

    elif data is not None:
        # Load data from list
        train_X_loaded = data[0]
        train_y_loaded = data[1]
        test_X_loaded = data[2]
        test_y_loaded = data[3]

    print('\nFormatting DMatrices...')
    dtrain = prepare_DMatrix(train_X_loaded, train_y_loaded, feat_names=train_names)
    dtest = prepare_DMatrix(test_X_loaded, test_y_loaded, feat_names=test_names)

    print('\nTraining...')
    m = build_model(dtrain, dtest)

    if save_dir is not None:
        pickle.dump(m, open(save_dir, 'wb'))

    return m, dtrain, dtest


def perform_mapping(mapper, training_set=None, test_set=None, train_only=False, test_only=False, silent=False):
    """ Maps dummy variables. """
    if train_only and test_only:
        raise ValueError("Only one of 'train_only' and 'test_only' can be True.")

    if not silent:
        print('\nStarting mappings...')
    start = time.time()

    if not test_only:
        train_X = mapper.fit_transform(training_set)
        train_y = training_set.price_perc.values
        train_names = mapper.transformed_names_

    if not train_only:
        test_X = mapper.transform(test_set)
        test_y = test_set.price_perc.values
        test_names = mapper.transformed_names_

    if not silent:
        print('Mappings executed in {} seconds'.format(round((time.time() - start), 2)))

    if train_only:
        return train_X, train_y, train_names
    if test_only:
        return test_X, test_y, test_names

    return train_X, train_y, test_X, test_y, train_names, test_names


def prepare_DMatrix(X, y, feat_names=None):
    """ Creates an XGBoost DMatrix """
    if not isinstance(y, float):
        y = y.astype(float)
    return xgb.DMatrix(X, label=y, feature_names=feat_names)


def build_model(dtrain, dtest):
    """ Function that builds the XGBoost model """
    # Specify parameters via dict
    param = {'eta': 0.3, 'objective': 'reg:linear', 'eval_train': 1, 'silent': 1, 'max_depth': 8,
             'eval_metric': ['rmse', 'mae']}
    num_round = 1000
    eval_list = [(dtrain, 'train'), (dtest, 'eval')]
    model = xgb.train(params=param, dtrain=dtrain, num_boost_round=num_round, evals=eval_list, early_stopping_rounds=1)

    return model


""" Functions for predicting with XGBoost"""


def predict(model, dtest):
    """ Function that makes price predictions using the XGBoost model """
    # Make prediction
    preds = model.predict(dtest)

    return preds


def forecast(df, model, mapper, periods, num_entries, method='step', save_dir=None):
    """ Forecasts the price of products in 'df' a number of days of 'periods' ahead.

    Params:
    ------
    df: pandas DataFrame
    model: XGBoost regression model
    mapper: Mapping used in the creation of the model
    periods: number of days ahead to forecast - can be list of periods as well
    num_entries: the number of past price entries given
    method: type of forecast. Options:
        - 'step' (aka recursive): predicts in steps of a day until the desired day is reached.
        - 'leap' (aka direct): makes the prediction of the desired day in one large leap
    save_dir: directory and filename to save file under, optional

    Returns:
    -------
    pandas DataFrame

    """

    if num_entries < 1:
        print("At least one previous price must be provided.")
        raise ValueError

    # Prepare the first-round data
    this_round = df.copy()
    preds = []

    try:
        this_round.loc[:, 'periods'] = periods
        this_round = this_round.dropna(subset=['periods'])
    except ValueError:
        print("\nException raised. Please ensure that 'df' and 'periods' have the same length, or that 'periods' is "
              "a non-iterable type. Current length of 'df': {} and 'periods': {}.".format(len(df), len(periods)))
        raise

    print('\nPredicting...')

    if method == 'step':
        try:
            periods = max(periods) + 1
        except TypeError:
            periods = periods

        for period in range(periods):
            # Format data
            this_round = format.created_at_to_dt(this_round, date_only=False)

            # Perform mappings
            X, y, test_names = perform_mapping(mapper, test_set=this_round, test_only=True, silent=True)
            data = prepare_DMatrix(X, y, feat_names=test_names)

            # Make predictions and store these
            prediction = pd.Series(predict(model, data))
            pred = pd.concat([this_round, prediction], axis=1).rename(columns={0: 'predictions'})
            preds.append(pred)

            # Filter products
            this_round = this_round[this_round['periods'] >= period + 1].reset_index(drop=True)

            if len(this_round) > 0:
                # Make preparations for the next round
                this_round.loc[:, 'days_from_first'] += 1
                this_round.loc[:, 'days_from_prev_1'] = 1
                this_round.loc[:, 'days_from_date'] += 1
                this_round.loc[:, 'prior_entries'] += 1
                this_round.loc[:, 'days_from_christmas'] = format.add_days_from_holidays(this_round, format.holidays)

                this_round_preds = pred['predictions']

                for entry in reversed(range(num_entries)):
                    if entry > 0:
                        this_round.loc[:, 'price_perc_prev_{}'.format(entry + 1)] = this_round[
                            'price_perc_prev_{}'.format(entry)]
                        this_round.loc[:, 'days_from_prev_{}'.format(entry + 1)] = this_round[
                            'days_from_prev_{}'.format(entry)]
                        this_round.loc[:, 'df_{}'.format(entry + 1)] = this_round['df_{}'.format(entry)]
                        if entry < num_entries - 1:
                            this_round.loc[:, 'ddf_{}'.format(entry + 1)] = this_round.loc[:, 'ddf_{}'.format(entry)]

                this_round.loc[:, 'price_perc_prev_1'] = this_round_preds  # Use predicted price for next round's price
                this_round.loc[:, 'ref_new_price'] = this_round['first_price'] * this_round_preds

                if num_entries > 1:
                    this_round.loc[:, 'df_1'] = ((this_round['price_perc_prev_1'] - this_round['price_perc_prev_2'])
                                                 / (this_round['days_from_prev_1']))
                    if num_entries > 2:
                        this_round.loc[:, 'ddf_1'] = 2 * ((df['df_1'] - df['df_2']) / (df['days_from_prev_2'] +
                                                                                       df['days_from_prev_3']))

                this_round.loc[:, 'created_at'] += datetime.timedelta(days=1)
                this_round_dt = this_round.created_at.dt
                this_round.loc[:, 'year'] = this_round_dt.year
                this_round.loc[:, 'season'] = format.add_season(this_round)
                this_round.loc[:, 'month'] = format.add_month(this_round, as_string=True)
                this_round.loc[:, 'week'] = this_round_dt.week
                this_round.loc[:, 'weekday'] = this_round_dt.weekday

                this_round.loc[:, 'avg_RoC'] = (1 - this_round['price_perc_prev_1']) / abs(this_round['days_from_first'])

    elif method == 'leap':
        for leap in range(periods + 1):
            X, y = perform_mapping(mapper, test_set=this_round, test_only=True, silent=True)
            data = prepare_DMatrix(X, y)

            # Make predictions and store these
            pred = pd.concat([this_round, pd.Series(predict(model, data))], axis=1).rename(columns={0: 'predictions'})
            preds.append(pred)

            # Make preparations for the next round
            this_round.loc[:, 'days_from_first'] += 1
            this_round.loc[:, 'days_from_prev_1'] += 1
            this_round.loc[:, 'created_at'] += datetime.timedelta(days=1)
            this_round.loc[:, 'month'] = this_round.created_at.dt.month

    fcast = pd.concat(preds, axis=0).reset_index(drop=True)
    if save_dir is not None:
        fcast.to_csv(save_dir)

    return fcast
