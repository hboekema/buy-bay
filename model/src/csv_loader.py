""" File for loading and manipulating CSV files """


from os.path import join
import pandas as pd
import numpy as np
from scipy.sparse import csr_matrix

import format


def load_csv(path, format_df=True, type_conversion=False, infer_dtype=False, drop_unnamed=True, cache=False,
             filename=None):
    """ Loads a CSV file and turns this into a pandas DataFrame """
    if infer_dtype:
        df = pd.read_csv(path, error_bad_lines=False)
    else:
        df = pd.read_csv(path, dtype=str, error_bad_lines=False)

    if format_df:  # Format the file - only useful for data retrieved using data_script.sql
        df.columns = ['service_orderline_price_changes_id', 'service_orderline_id', 'created_at', 'ref_new_price',
                      'competitor', 'stock_new_price', 'product_id', 'ean', 'article_code', 'description_nl',
                      'brand_id', 'internal_category_id']
        df.sort_values('product_id')

    if type_conversion:
        # Convert types to what they should be, not what they were inferred to be
        df = format.convert_feat_types(df)

    if drop_unnamed:
        try:
            df = df.drop('Unnamed: 0', axis=1)
        except ValueError:
            df = df

    if cache:
        if filename is None: filename = 'cached_df.csv'
        cache_path = join('cache/', filename)
        open(cache_path, 'w')

    return df


def join_df(df, other, df_key, other_key):
    """ Performs an inner join on two DataFrames using the specified column headers """
    return df.merge(other, how='left', left_on=df_key, right_on=other_key)


# Handle CSR matrix save/load operations
def save_sparse_csr(filename, array):
    # Note that .npz extension is added automatically
    np.savez(filename, data=array.data, indices=array.indices,
             indptr=array.indptr, shape=array.shape)


def load_sparse_csr(filename):
    # Here we need to add .npz extension manually
    loader = np.load(filename + '.npz')
    return csr_matrix((loader['data'], loader['indices'], loader['indptr']),
                      shape=loader['shape'])
