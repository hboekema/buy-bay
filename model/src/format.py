""" File for formatting data """


import datetime
import itertools

import pandas as pd
import numpy as np


""" Global constants """

holidays = [('christmas', datetime.datetime(2017, 12, 25))]
months_of_year = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',
                  'November', 'December']


""" Functions that make a train/test split on specific criteria """


def create_training_test_set(df, split_type='id', silent=True):
    """ Applies filters and creates the training and test set for XGBoost. 'split_type' options are 'id', 'time',
    'time_id' and (default) 'random'. """
    # Set columns to the correct types
    df = created_at_to_dt(df)
    df = id_to_int(df)
    df = price_to_float(df)

    # Filter duplicates, keeping only the lowest price for each product
    df = df.sort_values(by=['product_id', 'created_at', 'ref_new_price'], ascending=[True, True, True])
    df = df.drop_duplicates(subset=['product_id', 'created_at'], keep='first').reset_index(drop=True)

    # Add column 'first_price' to the DataFrame
    df = find_first_price(df)

    # Add column 'days_from_first' to the DataFrame
    df = days_from_first(df)

    # Add column 'price_perc' to the DataFrame
    df = add_price_perc(df)

    # Apply filters
    df = filter_by_price_range(df, max_range=0.5)
    df = filter_by_num_prices(df, min_cases=30)
    df = rm_gaps(df, gap_size=80)

    # Change 'mapped_category_id' lists to tuples and convert 'description_nl' to type str
    df['mapped_category_id'] = df['mapped_category_id'].apply(lambda x: tuple(x) if isinstance(x, list) else x)
    df = description_to_str(df)
    df = brand_id_to_str(df)

    # Add column 'weekday' to the DataFrame
    df = add_weekday(df)

    # Add column 'week' to the DataFrame
    df = add_week(df)

    # Add column 'months' to the DataFrame
    df = add_month(df, as_string=True)

    # Add column 'season' to the DataFrame
    df = add_season(df)

    # Add column 'year' to the DataFrame
    df = add_year(df)

    # Add column 'days from date' to the DataFrame
    df = add_days_from_date(df, datetime.date(2017, 1, 1))

    # Add column 'days from christmas' to the DataFrame
    df = add_days_from_holidays(df, holidays)

    # Add ordinal categorical variable grouping prices into classes to DataFrame
    df = add_price_class(df, start=0, stop=500, step=50, use_stock_price=True)

    # Change time format to be compatible with the model and prediction method
    df = change_time_format(df, format_type='step', entries_ago=5, rate_of_change=True)

    # Create training and test set
    if split_type == 'id':
        # Ensure that all entries of each product are in the same set
        training_set, test_set = create_id_subset(df, size=0.8)

    elif split_type == 'time':
        # Split the training and test data by 'created_at'
        training_set, test_set = create_time_subset(df, days=100)

    elif split_type == 'time_id':
        # Split by the recency of entries, by product id
        training_set, test_set = create_time_id_subset(df, size=0.8)

    else:
        # Simply create random samples
        training_set = df.sample(frac=0.8, replace=False)

        mask = df.index.isin(training_set.index)
        test_set = df[~mask]

    if not silent:
        print("Training set length: {}".format(len(training_set)))
        print("Test set length: {}\n".format(len(test_set)))

    return training_set, test_set


""" Functions that adjust the format of the DataFrame entries """


def change_time_format(df, format_type='default', entries_ago=1, rate_of_change=True):
    """ Changes the format of the time-related data of the DataFrame to a program-specified type.

    Params:
    ------
    df: pandas DataFrame
    format_type: the style desired for the data. Choices are:
    - 'default': the default formatting style. 'days_from_first' is measured relative to the first date and 'ref_new_price' is
                 the price pertaining to the current day.
    - 'step': 'days_from_first' indicates the days since the previous price point; 'ref_new_price' is the price pertaining to
              the current day. A new column 'price_perc_prev' will be added to the DataFrame containing the most recent
              previous price percentage.
    - 'first': perhaps the most controversial option. 'days_from_first' is taken in relation to the first date. However,
               column 'price_perc_initial' will be added to the DataFrame - this will always be 1.
    entries_ago: only to be used if format_type is 'step'. Indicates the number of previous prices to add to the data.
    rate_of_change: only to be used if format_type is 'step'. Calculates the rate of change between the past
                    'entries_ago' entries.

    Returns:
    -------
    pandas DataFrame

    """

    if format_type == 'default':
        df = days_from_first(df)
        df = add_price_perc(df)

    elif format_type == 'step':
        df = find_prev_prices(df, entries_ago)

        if rate_of_change is True:
            df = find_first_derivs(df, num_derivs=entries_ago - 1)
            df = find_second_derivs(df, num_derivs=entries_ago - 2)

            # Calculate the average rate of change since the first price point
            df.loc[:, 'avg_RoC'] = (1 - df['price_perc_prev_1']) / df['days_from_first']

    elif format_type == 'first':
        df = days_from_first(df)
        df.loc[:, 'price_perc_initial'] = 1.0

    else:
        print('Format not recognised. Original DataFrame returned.')

    return df


def find_prev_prices(df, num_prices):
    """ Finds the 'num_prices' previous prices and adds these to the DataFrame. """
    df = df.sort_values(by=['product_id', 'created_at'], ascending=[True, True]).reset_index(drop=True)

    try:
        df['price_perc']
    except KeyError:
        # Add 'price_perc'
        df = add_price_perc(df)

    # Track when the last price changes occurred
    df = days_from_prev(df, overwrite=False, entries_ago=num_prices)

    # Find the last few prices
    for entry in reversed(range(num_prices)):
        df.loc[:, 'price_perc_prev_{}'.format(entry + 1)] = df['price_perc'].shift(entry + 1)

        # Set 'price_perc_prev' for the first 'entries_ago' entries of each product to None
        grouped_df = df.groupby('product_id').head(entry + 1)
        mask = df.index.isin(grouped_df.index)
        df.loc[mask, 'price_perc_prev_{}'.format(entry + 1)] = None

    return df


def find_first_derivs(df, num_derivs):
    """ Finds the past 'num_derivs' rates of change of product prices. """
    df = df.sort_values(by=['product_id', 'created_at'], ascending=[True, True]).reset_index(drop=True)

    try:
        for i in range(1, num_derivs + 2):
            df['price_perc_prev_{}'.format(i)]
    except KeyError:
        df = find_prev_prices(df, num_prices=num_derivs + 1)

    # Track when the last price changes occurred
    df = days_from_prev(df, overwrite=False, entries_ago=num_derivs + 1)

    for entry in reversed(range(1, num_derivs + 1)):
        # Calculate the average rate of change of the past few prices
        df.loc[:, 'df_{}'.format(entry)] = (df.loc[:, 'price_perc_prev_{}'.format(entry)] -
                                            df.loc[:, 'price_perc_prev_{}'.format(entry + 1)]) / \
                                           (df.loc[:, 'days_from_prev_{}'.format(entry)])

    return df


def find_second_derivs(df, num_derivs):
    """ Finds the past 'num_derivs' second derivatives of product prices as a function of time. """
    df = df.sort_values(by=['product_id', 'created_at'], ascending=[True, True]).reset_index(drop=True)

    try:
        for i in range(1, num_derivs + 3):
            df['price_perc_prev_{}'.format(i)]
    except KeyError:
        df = find_prev_prices(df, num_prices=num_derivs + 2)

    # Track when the last price changes occurred
    df = days_from_prev(df, overwrite=False, entries_ago=num_derivs + 2)

    for entry in reversed(range(1, num_derivs + 1)):
        # Calculate the average rate of change of the average rate of change of the past few prices
        df.loc[:, 'ddf_{}'.format(entry)] = 2 * ((df.loc[:, 'df_{}'.format(entry)] -
                                                  df.loc[:, 'df_{}'.format(entry + 1)]) /
                                                 (df.loc[:, 'days_from_prev_{}'.format(entry + 1)] +
                                                  df.loc[:, 'days_from_prev_{}'.format(entry + 2)]))
    return df


""" Functions that handle time gaps in product entries """


def rm_gaps(df, gap_size=5):
    """ Removes gaps of at least size 'size' (per product id), keeping the longest stretch of data without
    such gaps. NOTE: Also drops duplicates of 'product_id' and 'days_from_first' """
    df = id_to_int(df)
    df = price_to_float(df)

    # Ensure that days_from_first was calculated
    try:
        df['days_from_first']
    except KeyError:
        df = days_from_first(df)

    df = df.sort_values(['product_id', 'days_from_first', 'ref_new_price'], ascending=[True, True, True])
    temp = df[['product_id', 'created_at', 'days_from_first']].copy()
    temp = temp.drop_duplicates(subset=['product_id', 'days_from_first'])

    # Sort by product_id, then by days_from_first
    temp = temp.sort_values(by=['product_id', 'days_from_first'], ascending=[True, True]).reset_index(drop=True)
    temp.loc[:, 'gap'] = temp['days_from_first'].diff()

    # A cunning construction to overcome the difficulty of negative gaps when there is a change in product_id
    temp.loc[(temp['gap'] < 0), 'gap'] = 0

    # Guard the location of gaps that exceed the limit
    temp.loc[:, 'mask'] = temp['gap'] >= gap_size

    # Set the mask for the first entry of each product_id to True
    first_occurrence = temp.drop_duplicates(subset=['product_id'], keep='first')
    mask = temp.index.isin(first_occurrence.index)
    temp.loc[mask, 'mask'] = True

    # Do the same for the last entry
    last_occurrence = temp.drop_duplicates(subset=['product_id'], keep='last')
    mask = temp.index.isin(last_occurrence.index)
    temp.loc[mask, 'mask'] = True

    # Count the number of data points between the last two gaps
    trues = temp['mask'].values == 1
    falses = ~trues

    counts = np.cumsum(falses)[trues]
    counts[1:] -= counts[:-1].copy()

    gap_counts = temp[trues]
    gap_counts.loc[:, 'count'] = counts

    # Necessary to preserve the index (which is needed in a later step)
    gap_counts.loc[:, 'i'] = gap_counts.index

    # Remove products with only one data point here
    occurrence_table = pd.pivot_table(gap_counts, index=['product_id'], values=['days_from_first'], aggfunc=len)
    occurrence_table = occurrence_table.unstack().reset_index().drop('level_0', axis=1)
    occurrence_table = occurrence_table[occurrence_table[0] >= 2]

    i1 = gap_counts.set_index('product_id').index
    i2 = occurrence_table.set_index('product_id').index

    gap_counts = gap_counts[i1.isin(i2)]

    # Find the longest stretch of data points and the dates these lie between
    max_counts = gap_counts.sort_values('count', ascending=False).groupby('product_id', as_index=False).first()
    max_counts = max_counts[max_counts['count'] > 0]  # Throw away entries with a count of 0

    # Format DataFrame
    gap_counts = gap_counts.sort_values(by=['product_id', 'days_from_first'], ascending=[True, True])

    prior_to_max = gap_counts['i'].isin(max_counts['i'])
    prior_to_max = prior_to_max.shift(-1).fillna(False)
    dates = pd.concat([gap_counts[prior_to_max], max_counts], axis=0).sort_values(by='product_id')

    # Store the indices of the two 'important' values per product id in a list of tuples
    indices = dates.sort_values(by='i', ascending=True).groupby('product_id')['i'].apply(tuple).reset_index()['i'] \
        .values

    del dates  # no longer useful

    concat_list = []

    # Now throw out any entries that do not fall between the aforementioned indices
    for index_tup in indices:
        concat_list.append(temp.iloc[index_tup[0]:index_tup[1]])

    # Create the final DataFrame
    no_gaps = pd.concat(concat_list, axis=0)

    # Create final DataFrame
    mask2 = df.index.isin(no_gaps.index)
    df = df[mask2].reset_index(drop=True)

    # Reset the 'days_from_first' and 'price_perc' columns
    try:
        # Drop column (if present)
        df = df.drop('first_price', axis=1)
    except ValueError:
        df = df

    df = add_price_perc(df)

    return df


""" Functions that filter the data on some criterion/criteria """


def filter_categories(categories_df, min_cases):
    """ Filters out categories that do not appear at least min_cases times. """
    categories_table = pd.pivot_table(categories_df, index='mapped_category_id', aggfunc=len)
    categories_table = categories_table.unstack().reset_index().drop('level_0', axis=1)
    mask = categories_df.index.isin(categories_table[categories_table[0] >= min_cases].index)

    return categories_df[mask]


def filter_by_num_prices(df, min_cases):
    """ Filters out products that do not have the requisite number of cases ('min_cases'). """
    # Remove products with fewer than a certain number of price entries
    price_table = pd.pivot_table(df, index=['product_id'], values=['ref_new_price'], aggfunc=len)
    price_table = price_table.unstack().reset_index().drop('level_0', axis=1)
    price_table = price_table[price_table[0] >= min_cases]

    # Only keep products in df that are also in (the filtered) price_table
    i1 = df.set_index('product_id').index
    i2 = price_table.set_index('product_id').index

    return df[i1.isin(i2)].reset_index(drop=True)


def filter_by_price_change(df, max_change=50):
    """ Calculates price change between two consecutive price entries for all price entries, per product_id. Then it
    filters out the last of two consecutive rows that have a greater than 'max_change' (a percentage) price change
    between them. Uses recursion and is as such VERY slow for large DataFrames (under stringent conditions). """
    # Prepare DataFrame by converting types etc.
    df = price_to_float(df)
    df = created_at_to_dt(df)

    initial_length = len(df)

    # Sort by product_id, then sort the prices chronologically
    temp_df = df.sort_values(by=['product_id', 'created_at'], ascending=[True, True]).copy()

    temp_df.loc[:, 'price_difference'] = temp_df['ref_new_price'].diff().fillna(0)

    # However, this causes difficulties when there is a change in product_id. Thus, filter out duplicates, keeping just
    # the first entry per DataFrame. Then, set 'price_difference' to 0.
    no_duplicates = temp_df.drop_duplicates(subset='product_id', keep='first')
    index = no_duplicates.index.tolist()

    temp_df.loc[index, 'price_difference'] = 0

    # Shift the price differences up a row
    temp_df.loc[:, 'price_difference'] = temp_df['price_difference'].shift(-1).fillna(0)

    # Now the percentage change (relative to the prior price) can be calculated
    temp_df.loc[:, 'perc_price_diff'] = temp_df['price_difference'] / temp_df['ref_new_price'] * 100

    # Shift back and filter out rows with price changes greater than 'max_change'
    temp_df.loc[:, 'perc_price_diff'] = temp_df['perc_price_diff'].shift(1).fillna(0)
    temp_df = temp_df[temp_df['perc_price_diff'] <= max_change]

    # Drop columns added by this function
    temp_df = temp_df.drop(labels=['price_difference', 'perc_price_diff'], axis=1)

    if len(temp_df) != initial_length:
        temp_df = filter_by_price_change(temp_df, max_change)

    return temp_df


def filter_by_price_range(df, max_range=0.4):
    """ Finds the range of prices of products in a DataFrame and filters out products with a range of more than
    'max_range' percent (as decimal) of the initial price. """
    df = id_to_int(df)

    try:
        df['price_perc']
    except KeyError:
        df = add_price_perc(df)

    # Find the range
    max = (df.groupby('product_id').max())['price_perc']
    min = (df.groupby('product_id').min())['price_perc']
    range_df = (max - min)

    # Perform the check and return all the entries that pass the condition
    check = range_df[range_df <= max_range]
    mask = df.product_id.isin(check.index)
    return df[mask].reset_index(drop=True)


""" Functions that add a column to the DataFrame """


def add_prior_entries(df, add=True):
    """ Adds the number of entries prior to each entry, grouped by 'product_id'. """
    df = id_to_int(df)
    df = price_to_float(df)
    df = created_at_to_dt(df)

    # Duplicates must be dropped for this function to work properly
    df = df.sort_values(by=['product_id', 'created_at', 'ref_new_price'], ascending=[True, True, True])
    df = df.drop_duplicates(subset=['product_id', 'created_at']).reset_index(drop=True)

    counts = df.groupby('product_id')['created_at'].nunique().values

    # Create and flatten lists - this is fairly inefficient
    counts = pd.Series(list(itertools.chain(*list(list(range(x)) for x in counts))))

    if add:
        df.loc[:, 'prior_entries'] = counts
        return df
    else:
        return counts


def add_price_perc(df, relative_to_first=True, transform=None, drop_price=False):
    """ Adds column with price as a percentage of the first price to the DataFrame. Param 'transform' allows user to
    apply a transformation function on this column. """
    if relative_to_first:
        try:
            df['first_price']
        except KeyError:
            df = find_first_price(df)

        df.loc[:, 'price_perc'] = df['ref_new_price'] / df['first_price']
        if drop_price:
            df = df.drop('first_price', axis=1)

    else:
        try:
            df['last_price']
        except KeyError:
            df = find_last_price(df)

        df.loc[:, 'price_perc'] = df['ref_new_price'] / df['last_price']
        if drop_price:
            df = df.drop('last_price', axis=1)

    if transform is not None:
        df.loc[:, 'price_perc'] = df.loc[:, 'price_perc'].apply(transform)

    return df


def add_weekday(df):
    """ Adds a column that indicates which day of the week the entry in the DataFrame was created. """
    df = created_at_to_dt(df, date_only=False)
    df.loc[:, 'weekday'] = df['created_at'].dt.weekday
    return df


def add_week(df):
    """ Adds a column with the week of the year in which the DataFrame entry was created. """
    df = created_at_to_dt(df, date_only=False)
    df.loc[:, 'week'] = df['created_at'].dt.week
    return df


def add_month(df, as_string=False):
    """ Adds a column with the month of the year in which the DataFrame entry was created. """
    df = created_at_to_dt(df, date_only=False)
    df.loc[:, 'month'] = df['created_at'].dt.month

    if as_string:
        for i in range(1, len(months_of_year) + 1):
            df.loc[df['month'] == i, 'month'] = months_of_year[i - 1]

    return df


def add_season(df):
    """ Adds seasons as strings. """
    try:
        df['month']
    except KeyError:
        df = add_month(df, as_string=True)

    months = months_of_year
    month = df['month']

    # Classify data
    winter = (month == 12) | (month == 1) | (month == 2) | \
             (month == months[11]) | (month == months[0]) | (month == months[1])
    spring = (month == 3) | (month == 4) | (month == 5) | \
             (month == months[2]) | (month == months[3]) | (month == months[4])
    summer = (month == 6) | (month == 7) | (month == 8) | \
             (month == months[5]) | (month == months[6]) | (month == months[7])
    autumn = (month == 9) | (month == 10) | (month == 11) | \
             (month == months[8]) | (month == months[9]) | (month == months[10])

    # Set categories
    df.loc[winter, 'season'] = 'winter'
    df.loc[spring, 'season'] = 'spring'
    df.loc[summer, 'season'] = 'summer'
    df.loc[autumn, 'season'] = 'autumn'

    return df


def add_year(df):
    """ Adds a column that indicates which year the entry in the DataFrame was created. """
    df = created_at_to_dt(df, date_only=False)
    df.loc[:, 'year'] = df['created_at'].dt.year
    return df


def add_days_from_date(df, date):
    """ Adds a column that indicates the difference between a date and the date the entry in the DataFrame was
    created. """
    df = created_at_to_dt(df, date_only=False)
    df.loc[:, 'days_from_date'] = (df['created_at']-date).dt.days
    return df


def add_days_from_holidays(df, holidays):
    """ Add columns containing the date to a holiday. Holidays name and dates should be provided through the
    'holidays' parameter in the form of a list of (name, date) tuples. """
    if not (isinstance(holidays, list) or isinstance(holidays, tuple)):
        raise TypeError('Incorrect parameter format')
    if not (all((isinstance(holiday, list) or isinstance(holiday, tuple)) for holiday in holidays)):
        raise TypeError('Incorrect parameter format')

    df = created_at_to_dt(df, date_only=False)

    for name, date in holidays:
        df.loc[:, 'days_from_{}'.format(name)] = (df['created_at'] - date).dt.days % -365
    return df


def days_from_first(df):
    """ Adds a column containing the number of days since the first entry of a product to the DataFrame """
    df = created_at_to_dt(df)
    df = df.reset_index(drop=True)

    no_duplicates = df.sort_values(by='created_at', ascending=True).drop_duplicates(subset='product_id', keep='first')

    first_price = no_duplicates[['product_id'] + ['created_at']]
    first_price.columns = ['product_id', 'first_date']
    merged_df = pd.merge(df, first_price, on='product_id', how='inner')

    df.loc[:, 'days_from_first'] = merged_df['created_at'] - merged_df['first_date']
    df.loc[:, 'days_from_first'] = df['days_from_first'].dt.days

    df = df.dropna(subset=['days_from_first']).reset_index(drop=True)
    df.loc[:, 'days_from_first'] = df.days_from_first.astype(int)

    return df


def days_from_last(df):
    """ Adds a column containing the number of days 'since' the last entry of a product to the DataFrame """
    df = created_at_to_dt(df)
    df = df.reset_index(drop=True)

    no_duplicates = df.sort_values(by='created_at', ascending=False).drop_duplicates(subset='product_id', keep='first')

    last_price = no_duplicates[['product_id'] + ['created_at']]
    last_price.columns = ['product_id', 'last_date']
    merged_df = pd.merge(df, last_price, on='product_id', how='inner')

    df.loc[:, 'days_from_last'] = merged_df['created_at'] - merged_df['last_date']
    df.loc[:, 'days_from_last'] = df['days_from_last'].dt.days

    df = df.dropna(subset=['days_from_last']).reset_index(drop=True)
    df.loc[:, 'days_from_last'] = df.days_from_first.astype(int)

    return df


def days_from_prev(df, overwrite=False, entries_ago=1, from_last=False):
    """ Adds a column containing the number of days since the previous entry of a product to the DataFrame. Param
    'entries_ago' can only be used if 'overwrite' is False. This parameter allows the user to calculate the days since
    from the last 'entries_ago' entries. 'from_last' allows this function to be used with function days_from_last(). """
    df = created_at_to_dt(df)
    df = id_to_int(df)

    try:
        df['days_from_first']
    except KeyError:
        df = days_from_first(df)

    df = df.sort_values(by=['product_id', 'created_at'], ascending=[True, True]).reset_index(drop=True)

    if overwrite:
        # Calculate days since last price point and format
        df.loc[:, 'days_from_first'] = df['created_at'].diff().dt.days.fillna(0).astype(int)

        # Overcomes difficulties that arise when there is a change in product id
        df.loc[df['days_from_first'] < 0, 'days_from_first'] = 0

        if from_last:
            df.loc[:, 'days_from_first'] = -df.loc[:, 'days_from_first']

    else:
        for entry in reversed(range(entries_ago)):
            # Calculate days since last price point and format
            df.loc[:, 'days_from_prev_{}'.format(entry + 1)] = df['created_at'].diff().dt.days.fillna(0).astype(int) \
                .shift(entry)

            # Overcomes difficulties that arise when there is a change in product id
            df.loc[df['days_from_prev_{}'.format(entry + 1)] < 0, 'days_from_prev_{}'.format(entry + 1)] = None

            if from_last:
                df.loc[:, 'days_from_prev_{}'.format(entry + 1)] = -df.loc[:, 'days_from_prev_{}'.format(entry + 1)]

    return df


def find_first_price(df):
    """ Finds the first (and lowest if multiple prices on day 0) price of each product and adds it to the DataFrame. """
    df = price_to_float(df)
    df = created_at_to_dt(df)

    no_duplicates = df.sort_values(by=['product_id', 'created_at', 'ref_new_price'], ascending=[True, True, True]) \
        .drop_duplicates(subset='product_id', keep='first')

    first_price = no_duplicates[['product_id'] + ['ref_new_price']]
    first_price.columns = ['product_id', 'first_price']

    try:
        # Allows column 'first_price' to be overwritten
        df = df.drop('first_price', axis=1)
    except ValueError:
        df = df

    return pd.merge(df, first_price, on='product_id').reset_index(drop=True)


def find_last_price(df):
    """ Finds the last (lowest if multiple prices on last day) price of each product and adds this to the DataFrame. """
    df = price_to_float(df)
    df = created_at_to_dt(df)

    no_duplicates = df.sort_values(by=['product_id', 'created_at', 'ref_new_price'], ascending=[True, False, True]) \
        .drop_duplicates(subset='product_id', keep='first')

    last_price = no_duplicates[['product_id'] + ['ref_new_price']]
    last_price.columns = ['product_id', 'last_price']

    return pd.merge(df, last_price, on='product_id').reset_index(drop=True)


def add_price_class(df, start=0, stop=400, step=50, use_stock_price=False):
    """ Makes the 'first_price' of each entry into an ordinal categorical variable and adds this to the DataFrame.
    The parameters represent the DataFrame, start price, stop price and range size, respectively. Also has option
    to use stock price instead of first price. """
    # Prepare data
    df = price_to_float(df)
    df = created_at_to_dt(df)

    if use_stock_price:
        # Find first 'stock_new_price' per product and class products using this
        df.sort_values(['product_id', 'created_at'], ascending=[True, True])
        df_copy = df.copy()
        uniques = df_copy.drop_duplicates('product_id', keep='first')
        uniques = uniques[['product_id', 'stock_new_price']]
        merged_copy = pd.merge(df_copy.drop('stock_new_price', axis=1), uniques, how='left', on='product_id')
        price = merged_copy['stock_new_price'].astype(float)
    else:
        # Use first 'ref_new_price' to perform classings
        try:
            df['first_price']
        except KeyError:
            df = find_first_price(df)

        price = df['first_price']

    ordinal_num = 0
    price_class_list = []

    for price_low_bound in range(start, stop, step):
        # Assign a class to the entries with prices falling between each range
        temp = df[(price > price_low_bound) & (price < price_low_bound + step)]
        if len(temp) > 0:
            temp.loc[:, 'price_class'] = ordinal_num
            price_class_list.append(temp)
        ordinal_num += 1

    # Deal with products with prices beyond 'stop'
    expensive_products = df[price > stop]
    if len(expensive_products) > 0:
        expensive_products.loc[:, 'price_class'] = ordinal_num
        price_class_list.append(expensive_products)

    return pd.concat(price_class_list, axis=0).reset_index(drop=True)


""" Functions that create data subsets on specific criteria """


def create_id_subset(df, size=0.8):
    """ Create subset of given DataFrame, splitting on 'product_id'. Size is a decimal of total DataFrame length """
    if not 0 <= size <= 1:
        raise ValueError("Parameter 'size' must be a float between 0.0 and 1.0 inclusive")

    # Sort DataFrame on product_id
    df = id_to_int(df)

    prod_ids = pd.Series(df['product_id'].unique())
    id_sample = prod_ids.sample(frac=0.8)
    print(id_sample)

    mask = df.product_id.isin(id_sample.values)
    set_1 = df[mask].reset_index(drop=True)
    set_2 = df[~mask].reset_index(drop=True)

    return set_1, set_2


def create_time_subset(df, days=30):
    """ Creates subsets of given DataFrame, splitting on 'created_at'. Days is the point at which to make the split. """
    if days < 0:
        raise ValueError("Parameter 'days' must be a positive integer")

    # Ensure 'created_at' is the right format
    df = created_at_to_dt(df)

    newest = df.sort_values(by='created_at', ascending=False)['created_at'].values[0]

    # Filter out entries with a 'days_from_first' of greater than 'days'
    mask = df['created_at'] >= (newest - datetime.timedelta(days))
    newest = df[mask]
    oldest = df[~mask]  # The other data subset

    return oldest.reset_index(drop=True), newest.reset_index(drop=True)


def create_time_id_subset(df, size=0.8):
    """ Creates a subset of the first 'size' data points (as decimal of total) per product id. """
    if not 0 <= size <= 1:
        raise ValueError("Parameter 'size' must be a float between 0.0 and 1.0 inclusive")

    df = created_at_to_dt(df, date_only=True)
    df = id_to_int(df)

    try:
        df['days_from_first']
    except KeyError:
        df = days_from_first(df)

    try:
        df['price_perc']
    except KeyError:
        df = add_price_perc(df)

    # Duplicates must be dropped for this function to work properly
    df = df.sort_values(by=['product_id', 'created_at', 'price_perc'], ascending=[True, True, True])
    df = df.drop_duplicates(subset=['product_id', 'created_at'], keep='first').reset_index(drop=True)

    # Find the most recent entries and create a 'boundary' entry
    newest_entries = df.groupby('product_id')['days_from_first'].max().reset_index()
    newest_entries = newest_entries.rename(columns={'days_from_first': 'newest_entry'})
    temp = df.merge(newest_entries, on='product_id', how='left')
    temp.loc[:, 'boundary_entry'] = (temp['newest_entry'] * size).astype(int)

    # Make the split
    mask = temp['days_from_first'] <= temp['boundary_entry']
    oldest = temp[mask]
    newest = temp[~mask]

    oldest = oldest.drop(labels=['newest_entry', 'boundary_entry'], axis=1)
    newest = newest.drop(labels=['newest_entry', 'boundary_entry'], axis=1)

    return oldest.reset_index(drop=True), newest.reset_index(drop=True)


""" Functions that facilitate the creation of a certain type of test set for the ML model. Also fall under above
category. """


def min_density_set(df, days=100, density=0.8):
    """ Creates a test set with products that have a minimum data density of 'density' (given as decimal) over a period
    of the first 'days' days. """
    dense_df = created_at_to_dt(df)
    dense_df = id_to_int(dense_df)
    dense_df = price_to_float(dense_df)

    # Ensure that 'days_from_first' was calculated relative to first product appearance
    try:
        dense_df['days_from_first']
    except KeyError:
        dense_df = days_from_first(dense_df)

    # Ensure that only the lowest price per day is considered
    dense_df = dense_df.sort_values(by=['product_id', 'created_at', 'ref_new_price'], ascending=[True, True, True])
    dense_df = dense_df.drop_duplicates(subset=['product_id', 'created_at']).reset_index(drop=True)

    # Filter out all prices that were found at dates beyond the maximum permitted
    dense_df = dense_df[dense_df['days_from_first'] <= days]

    # Now check which products do not meet the density requirement and filter these out
    dense_table = pd.pivot_table(dense_df, index='product_id', values='ref_new_price', aggfunc=len)
    dense_table = dense_table.unstack().reset_index().drop('level_0', axis=1).rename(columns={0: 'count'})

    dense_table = dense_table[dense_table['count'] >= density * days].reset_index(drop=True)

    # Filter products out of DataFrame
    mask = dense_df.product_id.isin(dense_table.product_id)
    dense_df = dense_df[mask]

    print("Length of custom set: {}".format(len(dense_df)))

    return dense_df


def first_n_days_set(df, num_days):
    """ Filters products and entries out of a DataFrame that do not have a price point in the first 'num_days' days. """
    try:
        df['days_From_first']
    except KeyError:
        df = days_from_first(df)

    df = df.drop_duplicates(subset=['product_id', 'days_from_first'])  # Drop these duplicates - they are not useful
    df = df[df['days_from_first'] <= num_days - 1].reset_index(drop=True)  # Data beyond this is redundant
    df = id_to_int(df)

    copy = df.copy()

    # Find the days since the previous price point and drop any with a 'days_from_prev' of greater than 1
    copy = days_from_prev(copy, overwrite=False)
    copy = copy[copy['days_from_prev_1'] <= 1]

    # Clever part - group by 'product_id', count how many data points there are for each of these. If this is less than
    # 'num_days', this product does not meet the requirement and will be filtered.
    copy_table = pd.pivot_table(copy, index='product_id', values='ref_new_price', aggfunc=len)
    copy_table = copy_table.unstack().reset_index().drop('level_0', axis=1).rename(columns={0: 'count'})
    copy_table = copy_table[copy_table['count'] == num_days].reset_index(drop=True)

    mask = df.product_id.isin(copy_table.product_id)
    df = df[mask]
    print("Length of custom set: {}".format(len(df)))

    return df


""" Functions that reformat the data for aesthetic purposes """


def change_start_pos(df):
    """ Makes first entry per product occur on day 0 with a 'price_perc' of 1.0 """
    df = days_from_first(df)
    df = find_first_price(df)
    df = add_price_perc(df)

    return df


def reformat_preds(preds, test_set=None):
    """ Makes first entry per product occur on day 0 with 'predictions' scaled relative to the first price entry (in
     'preds') for each product. """
    if test_set is None:
        # Much more inaccurate scaling than if a test_set is provided!!
        preds = days_from_first(preds)
        preds.loc[:, 'first_price_old'] = preds['first_price']
        preds = find_first_price(preds)
        preds = add_price_perc(preds)

        factor = preds['first_price'] / preds['first_price_old']
        preds = preds.drop('first_price_old', axis=1)

        try:
            # Scale predictions
            preds.loc[:, 'predictions'] = preds['predictions'] * factor
        except KeyError:
            print('\nPredictions must have been made in order for this function to work.')
            raise
    else:
        # Test set must have been predicted on, otherwise this function will not work as expected
        test_set_copy = test_set.copy()
        test_first_price = test_set_copy.sort_values(['product_id', 'days_from_first'], ascending=[True, True])\
            .drop_duplicates('product_id')
        test_first_price = test_first_price[['product_id', 'price_perc']]
        test_first_price.columns = ['product_id', 'first_perc']

        preds = preds.merge(test_first_price, on='product_id')

        try:
            # Scale predictions
            preds.loc[:, 'predictions'] = preds['predictions'] / preds['first_perc']
        except KeyError:
            print('\nPredictions must have been made in order for this function to work.')
            raise

        # Drop unwanted/inaccurate columns
        preds = preds.drop(['first_perc', 'price_perc'], axis=1)
        preds = days_from_first(preds)

    return preds


""" Helper functions for the evaluate and model modules """


def infer_periods(df, start=0):
    """ Infers how many periods to predict prices for (for each product in the DataFrame). Returns a list of ints. """
    try:
        df['days_from_first']
    except KeyError:
        df = add_prior_entries(df, add=True)

    # Find number of entries for each product
    grouped_df = df.groupby('product_id')['days_from_first']
    periods = grouped_df.max().values - grouped_df.min().values
    forecast_range = np.asarray(periods - start)
    forecast_range[forecast_range < 0] = 0  # For safety

    return forecast_range


def split_by_change(df, column='price_perc'):
    """ Splits DataFrame into two DataFrames, with products with a net positive price change placed in one and those
     with a net negative change placed in the other. """
    df = id_to_int(df)
    df = created_at_to_dt(df)

    # Check that user is being sensible
    try:
        df.loc[:, column] = df[column].astype(float)
    except KeyError:
        print("\nColumn not recognised. Ensure that column '{}' is in the DataFrame.".format(column))
        raise

    except ValueError:
        print("\nColumn must have dtype convertible to type float.")
        raise

    # Find the average price change over all of each product's entries
    df = df.sort_values(['product_id', 'created_at'], ascending=[True, True]).reset_index(drop=True)
    grouped_df = df.groupby('product_id')[column]
    price_range = pd.DataFrame(grouped_df.tail(1).reset_index(drop=True) - grouped_df.head(1).reset_index(drop=True))
    price_range.loc[:, 'product_id'] = df['product_id'].unique()

    # Split into positive and negative changes
    pos_change = price_range[price_range[column] >= 0].product_id
    neg_change = price_range[price_range[column] < 0].product_id

    pos_df = df[df.product_id.isin(pos_change.product_id)].reset_index(drop=True)
    neg_df = df[df.product_id.isin(neg_change.product_id)].reset_index(drop=True)

    return pos_df, neg_df


""" Format types """


def convert_feat_types(df, date_only=True):
    """ Applies all of the below conversions. WARNING: does not add features of itself - if a feature is absent an
    exception will be raised. """
    df = created_at_to_dt(df, date_only=date_only)
    df = id_to_int(df)
    df = brand_id_to_str(df)
    df = price_to_float(df)
    df = days_to_int(df)
    df = description_to_str(df)
    df = cat_ids_to_str(df)
    df = year_to_str(df)

    return df


def created_at_to_dt(df, date_only=True):
    """ Converts 'created_at' column in DataFrame to type 'datetime'. Optionally only returns the date. """
    if not isinstance(df.created_at.dtype, datetime.datetime):
        # May raise an exception - this is deliberately left unhandled
        df.loc[:, 'created_at'] = pd.to_datetime(df.created_at)
    if date_only and not isinstance(df.created_at.dtype, datetime.date):
        # Returns type 'object'
        df.loc[:, 'created_at'] = df.created_at.dt.date

    return df


def year_to_str(df):
    if not isinstance(df.year.dtype, int):
        df.loc[:, 'year'] = df.year.astype(int)

    return df


def id_to_int(df):
    """ Converts 'product_id' column in DataFrame to type 'int'. """
    if not isinstance(df.product_id.dtype, int):
        # May raise an exception - this is deliberately left unhandled
        df.loc[:, 'product_id'] = df.product_id.astype(int)

    return df


def brand_id_to_str(df):
    """ Converts 'brand' column in DataFrame to type 'str'. """
    if not isinstance(df.brand_id.dtype, str):
        # May raise an exception - this is deliberately left unhandled
        df.loc[:, 'brand_id'] = df.brand_id.astype(str)

    return df


def price_to_float(df, convert_stock_price=False):
    """ Converts 'ref_new_price' and 'stock_new_price' (and potentially also 'price_perc') column in DataFrame to type
    'float'. """
    if not isinstance(df.ref_new_price.dtype, float):
        # May raise an exception - this is deliberately left unhandled
        df.loc[:, 'ref_new_price'] = df.ref_new_price.astype(float)
    if convert_stock_price and not isinstance(df.stock_new_price.dtype, float):
        # May raise an exception - this is deliberately left unhandled
        df.loc[:, 'stock_new_price'] = df.stock_new_price.astype(float)
    try:
        if not isinstance(df.price_perc.dtype, float):
            df.loc[:, 'price_perc'] = df.price_perc.astype(float)
    except (KeyError, AttributeError):
        df = df
    return df


def days_to_int(df):
    """ Converts 'days_from_first' column in DataFrame to type 'int'. """
    if not isinstance(df.days_from_first.dtype, int):
        # May raise an exception - this is deliberately left unhandled
        df.loc[:, 'days_from_first'] = df.days_from_first.astype(int)

    return df


def description_to_str(df):
    """ Converts 'description_nl' column in DataFrame to type 'str'. """
    if not isinstance(df.description_nl.dtype, str):
        # May raise an exception - this is deliberately left unhandled
        df.loc[:, 'description_nl'] = df.description_nl.astype(str)

    return df


def cat_ids_to_str(df):
    """ Converts entries in 'mapped_category_id' column in DataFrame to type 'str'. """
    df.loc[:, 'mapped_category_id'] = df.mapped_category_id.apply(lambda x: (str(entry) for entry in x))

    return df


def entry_to_array(entry, dtype=str):
    """ Converts entries to an array with types of 'dtype'. """
    return np.asarray(entry, dtype=dtype)
