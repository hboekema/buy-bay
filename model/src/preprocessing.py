from collections import defaultdict
import array
import pandas as pd
import numpy as np
import scipy.sparse as sp

from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.utils.validation import check_is_fitted
from sklearn.preprocessing import MultiLabelBinarizer


def _get_mask(X, value):
    """
    Compute the boolean mask X == missing_values.
    """
    if value == "NaN" or \
       value is None or \
       (isinstance(value, float) and np.isnan(value)):
        return pd.isnull(X)
    else:
        return X == value


def _get_series(X):
    if isinstance(X, pd.Series):
        return X
    return pd.Series(X)


class DataTypeSaver(BaseEstimator, TransformerMixin):
    """
    Save the input data type
    ----------

    Attributes
    ----------
    type_ : mixed
       The saved data type
    """

    def __init__(self):
        pass

    def fit(self, X, y=None):
        """
        Prepare replacement - no real action here
        Parameters
        ----------
            X : np.ndarray or pd.Series
                Training data.
            y : Passthrough for ``Pipeline`` compatibility.
        Returns
        -------
            self: DataTypeSaver
        """
        series = None

        self.type_ = "?"

        if type(X) == np.ndarray:
            if X.ndim > 1:
                # @TODO; work with multi-dimensional data
                self.type_ = "multidimensional"
                return self
            series = pd.Series(X)
        if type(X) == pd.Series:
            series = X

        if series is not None:
            self.type_ = str(type(series[0]))

        return self

    def transform(self, X):
        """
        Just passes through the data

        Parameters
        ----------
            X : np.ndarray or pd.Series
        Returns
        -------
            np.ndarray
                Data with imputed values.
        """

        check_is_fitted(self, 'type_')
        return np.asarray(X)


class NaFiller(BaseEstimator, TransformerMixin):
    """
    Replace missing data with a specific value.
    Parameters
    ----------
    missing_values : string or "NaN", optional (default="NaN")
        The placeholder for the missing values. All occurrences of
        `missing_values` will also be replaced with replacement. None and np.nan are treated
        as being the same, use the string value "NaN" for them.
    replacement : replacement value
    copy : boolean, optional (default=True)
        If True, a copy of X will be created.
    Attributes
    ----------
    fill_ : mixed
        The replacement given
    """

    def __init__(self, replacement, missing_values='NaN', copy=True):
        self.missing_values = missing_values
        self.replacement = replacement
        self.copy = copy

    def fit(self, X, y=None):
        """
        Prepare replacement - no real action here
        Parameters
        ----------
            X : np.ndarray or pd.Series
                Training data.
            y : Passthrough for ``Pipeline`` compatibility.
        Returns
        -------
            self: NaFiller
        """

        self.fill_ = self.replacement

        return self

    def transform(self, X):
        """
        Replaces missing values in the input data with the predetermined replacement.

        Parameters
        ----------
            X : np.ndarray or pd.Series
                Data with values to be imputed.
        Returns
        -------
            np.ndarray
                Data with imputed values.
        """

        check_is_fitted(self, 'fill_')

        if self.copy:
            X = X.copy()

        # mask indicate NA positions
        mask = _get_mask(X, self.missing_values)
        X[mask] = self.fill_

        return np.asarray(X)


class InfrequentItemReplacer(BaseEstimator, TransformerMixin):
    """
    Replace any values that do not occur at least min_frequency times.
    Ignores missing values (i.e. does not replace missing values in infrequent groups)

    Parameters
    ----------
    min_frequency : n integer with minimum frequency that an item should have
    num_groups : n integer with the maximum number of distinct original values to keep, the resulting number of groups will be num_groups + 1
    replacement : mixed

    copy : boolean, optional (default=True)
        If True, a copy of X will be created.

    Attributes
    ----------
    replace_ : the items that will be replaced

    @TODO: adhere to sklearn estimators best practices: http://scikit-learn.org/stable/developers/contributing.html#estimators
    and use default values (instantiate w/o arguments is allowed) and do input validation in fit
    """
    def __init__(self, replacement, min_frequency=None, num_groups=None, copy=True):
        if min_frequency is None and num_groups is None or (min_frequency is not None and num_groups is not None):
            raise ValueError("either min_frequency or num_groups must be set (but not both)")

        self.min_frequency = min_frequency
        self.num_groups = num_groups
        self.replacement = replacement
        self.copy = copy

    def fit(self, X, y=None):
        """

        Find the items that will be replaced

        Parameters
        ----------
            X : np.ndarray or pd.Series
                Training data.

            y : Passthrough for ``Pipeline`` compatibility.

        Returns
        -------
            self: InfrequentItemReplacer
        """

        counts = _get_series(X).value_counts()

        if self.min_frequency is not None:
            self.replace_ = counts[counts < self.min_frequency].keys().values
        if self.num_groups is not None:

            if self.num_groups >= len(counts):
                self.replace_ = np.ndarray(0)
            else:
                # get the last (total - num groups) groups to drop in transform
                self.replace_ = counts.keys().values[-(len(counts)-self.num_groups):]

        return self

    def transform(self, X):
        """

        Replaces missing values in the input data with the most frequent value
        of the training data.

        Parameters
        ----------
            X : np.ndarray or pd.Series
                Data with values to be imputed.

        Returns
        -------
            np.ndarray
                Data with imputed values.
        """

        check_is_fitted(self, 'replace_')

        if self.copy:
            X = X.copy()

        if len(self.replace_) > 0:
            X = _get_series(X).replace(self.replace_, self.replacement)

        return np.asarray(X)


class CategoricalImputer(BaseEstimator, TransformerMixin):
    """
    Impute missing values from a categorical/string np.ndarray or pd.Series
    with the most frequent value on the training data.

    Parameters
    ----------
    missing_values : string or "NaN", optional (default="NaN")
        The placeholder for the missing values. All occurrences of
        `missing_values` will be imputed. None and np.nan are treated
        as being the same, use the string value "NaN" for them.

    copy : boolean, optional (default=True)
        If True, a copy of X will be created.

    Attributes
    ----------
    fill_ : str
        Most frequent value of the training data.

    """

    def __init__(self, missing_values='NaN', copy=True):
        self.missing_values = missing_values
        self.copy = copy

    def fit(self, X, y=None):
        """

        Get the most frequent value.

        Parameters
        ----------
            X : np.ndarray or pd.Series
                Training data.

            y : Passthrough for ``Pipeline`` compatibility.

        Returns
        -------
            self: CategoricalImputer
        """

        mask = _get_mask(X, self.missing_values)
        X = X[~mask]

        modes = pd.Series(X).mode()
        if modes.shape[0] == 0:
            raise ValueError('No value is repeated more than '
                             'once in the column')
        else:
            self.fill_ = modes[0]

        return self

    def transform(self, X):
        """

        Replaces missing values in the input data with the most frequent value
        of the training data.

        Parameters
        ----------
            X : np.ndarray or pd.Series
                Data with values to be imputed.

        Returns
        -------
            np.ndarray
                Data with imputed values.
        """

        check_is_fitted(self, 'fill_')

        if self.copy:
            X = X.copy()

        mask = _get_mask(X, self.missing_values)
        X[mask] = self.fill_

        return np.asarray(X)


class WithUnseenMultiLabelBinarizer(MultiLabelBinarizer):
    """
    Extends sklearn MultiLabelBinarizer to make transform ignore labels not known at fit time (instead of throw an exception)
    """

    def _transform(self, y, class_mapping):
        """Transforms the label sets with a given mapping

        Parameters
        ----------
        y : iterable of iterables
        class_mapping : Mapping
            Maps from label to column index in label indicator matrix

        Returns
        -------
        y_indicator : sparse CSR matrix, shape (n_samples, n_classes)
            Label indicator matrix
        """
        indices = array.array('i')
        indptr = array.array('i', [0])
        for labels in y:
            if not isinstance(class_mapping, defaultdict):
                # this means we have already fitted, and classes have been fixed
                # now we filter out unseen classes
                labels = set(labels).intersection(class_mapping.keys())

            ext_set = set(class_mapping[label] for label in labels)
            indices.extend(ext_set)
            indptr.append(len(indices))
        data = np.ones(len(indices), dtype=int)

        return sp.csr_matrix((data, indices, indptr), shape=(len(indptr) - 1, len(class_mapping)))
