import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
#from . import tools  # helper
from preprocessing import DataTypeSaver
from datetime import datetime
import json
from timeit import default_timer as timer
import uuid
import sklearn_pandas


# default_model_filename
# default_transformer_filename
# default_config_filename

def get_features(mapper):
    """ Get the feature names and associated data type names of a transformer
    because we have given them the DataTypeSaver-helper during fit (or ? if type unknown)

    Parameters
    ----------
        mapper : sklearn_pandas.DataFrameMapper
    Returns
    -------
        dict
          {'category_id': "<class 'list'>", 'first_pricechange_comp_stock': "<class 'numpy.bool_'>"}
    """
    fields_dict = {}
    for feat in mapper.features:
        field_type = '?'

        key = feat[0]
        item = feat[1]
        if type(item) == sklearn_pandas.pipeline.TransformerPipeline:
            transformer = feat[1].steps[0][1]
        else:
            transformer = item

        if isinstance(transformer, DataTypeSaver):
            field_type = transformer.type_

        fields_dict[key] = field_type

    return fields_dict


class TrainingRunParameters(object):
    """
    Hold training parameters and time run
    Methods
    ----------
    export_parameters : export json to file
    Attributes
    ----------
    params : dict
        Holds the params
    """

    run_type = ""

    start = None

    export_prefix = ""

    export_path = ""

    run_id = ""

    params = {}

    def __init__(self, run_type="marketplace", export_path=".", run_id=None):
        self.run_type = run_type
        if run_id is None:
            self.run_id = datetime.now().strftime("%y%m%d%H") + '_' + str(uuid.uuid4())[0:3]
        else:
            self.run_id = run_id
        self.export_prefix = self.run_id + '_' + self.run_type + "_"

        self.export_path = export_path

        # run-id given, we need to load params from disk
        if run_id is not None:
            self.init_parameters_from_disk()

        self.start = timer()
        self.params['run_type'] = self.run_type
        self.params['export_prefix'] = self.export_prefix

    def get_export_path(self, filename):
        return self.export_path + "/" + self.export_prefix + filename

    def json_fix_numpy(self, o):
        if isinstance(o, np.integer): return int(o)
        raise TypeError

    def init_parameters_from_disk(self):
        outpath = self.get_export_path('preprocessing_parameters.json')
        with open(outpath) as data_file:
            self.params = json.load(data_file)

        tools.getLogInstance().info(type(self).__name__ + ": Initialised params from %s" % outpath)

    def dump_parameters(self):
        outpath = self.get_export_path('preprocessing_parameters.json')
        with open(outpath, 'w') as outfile:
            json.dump(self.params, outfile, default=self.json_fix_numpy)

        tools.getLogInstance().info(type(self).__name__ + ": Saved params to %s" % outpath)

    def get_elapsed_time(self):
        return timer() - self.start


def get_transformed_columns(mapper, mapper_input_df):
    """
    Get the columns that were transformed by this datatablemapper
    ----------
    mapper : sklearn_pandas.DataFrameMapper
    mapper_input_df : pandas.DataFrame that was used as training input

    Returns
    -------
    list
    """
    prior_df_out = mapper.df_out
    prior_sparse = mapper.sparse
    mapper.df_out = True
    mapper.sparse = False
    cols = list(mapper.transform(mapper_input_df.tail(1)).columns)
    mapper.df_out = prior_df_out
    mapper.sparse = prior_sparse
    return cols


def append_in_test_bool(sku_info, train_test_info):
    """
    Append an in_test boolean column to a df in order to be able to keep the same train/test split
    throughout different runs with support for unseen data

    @TODO write final split to file in order to also keep new split
    """
    assert 'in_test' not in list(sku_info.columns), "Already has in_test col"
    assert 'type' in list(train_test_info.columns), "type column in train_test_info"

    del train_test_info['X1']
    train_test_info.rename(columns={'warehouse_sku_id': 'skus_id'}, inplace=True)
    train_test_info['in_test'] = train_test_info['type'] == 'test'
    del train_test_info['type']

    sku_info = sku_info.merge(train_test_info, how='left', on='skus_id')

    ## all new skus since last split
    new_skus = sku_info[pd.isnull(sku_info['in_test'])]['skus_id'].unique()

    ## Split new skus previously unseen
    train, test = train_test_split(new_skus, test_size=.2)

    # create new dfs
    test_new = pd.DataFrame(test)
    test_new = test_new.rename(columns={0: 'skus_id'})
    test_new['in_test'] = 1

    train_new = pd.DataFrame(train)
    train_new = train_new.rename(columns={0: 'skus_id'})
    train_new['in_test'] = 0

    train_test_info = train_test_info.append(test_new).append(train_new)
    # since we merge the whole list back we dont need the existing type column that is incomplete for new obs
    del sku_info['in_test']
    sku_info = sku_info.merge(train_test_info, how='left', on='skus_id')

    # result should be empty now - all skus classified in train or test
    num_missing_skus = sku_info[pd.isnull(sku_info['in_test'])].shape[0]
    assert num_missing_skus == 0, "There are %d skus not assigned test/train" % num_missing_skus

    # @TODO write back split of new train/test skus to file

    return sku_info


def drop_missing(dataframe, column_list, scope_info=''):
    """
    Drop dataframe rows that have nas in column_list and log about it
    Parameters
    ----------
    dataframe : Pandas DataFrame
    column_list : list of columns that should not be removed
    scope_info : string, optional add to logging

    Returns
    -------
    pd.DataFrame
    """
    prior_rows = dataframe.shape[0]
    dataframe.dropna(subset=column_list, inplace=True)
    dropped_rows = prior_rows - dataframe.shape[0]
    tools.getLogInstance().info(scope_info + "dropped %.2f (%d out of %d) remaining rows with missing %s" % (
    dropped_rows / prior_rows, dropped_rows, prior_rows, str(column_list)))
    return dataframe


def drop_index(dataframe, drop_index, scope_info='', reason=''):
    """
    Drop dataframe rows along supplied index and log about it
    Parameters
    ----------
    dataframe : Pandas DataFrame
    drop_index : index that will be removed from the df
    scope_info : string, optional added to log
    reason :  string, optional added to log

    Returns
    -------
    pd.DataFrame
    """
    prior_rows = dataframe.shape[0]
    dataframe = dataframe[~drop_index]
    dropped_rows = prior_rows - dataframe.shape[0]
    tools.getLogInstance().info(scope_info + "dropped %.2f (%d out of %d) remaining rows %s" % (
    dropped_rows / prior_rows, dropped_rows, prior_rows, reason))
    return dataframe


######## CATEGORY HELPER FUNCTIONS

def get_categories_table(sql, con):
    use_csv = False

    if use_csv:
        ### currently use csv!
        cat_path = './_temp_cat_table.csv'
        try:
            cat_table = pd.read_csv(cat_path)
        except FileNotFoundError:
            cat_table = pd.read_csv("https://www.dropbox.com/s/v9fyq12ctxk5puv/product_category_mappings%20.csv?dl=1")
            cat_table.to_csv(cat_path)
        finally:
            tools.getLogInstance().warning("Using temporary category table: fix")

        return cat_table
    ### eof temp cat fix

    return sql.read_sql("""
             SELECT product_id AS products_id, category_id FROM view_category_products
"""
                        , con)


def add_category_list(price_points, category_list, min_cases_feature_creation):
    df = get_category_list_df(category_list, min_cases_feature_creation)
    return (price_points.merge(df, on=['product_id'], how='left'))


def get_use_cats(categories_table, min_cat_cases):
    # filter categories with at least x cases

    cats = pd.DataFrame(categories_table.dropna().pivot_table(index='mapped_category_id', values='product_id', aggfunc=len))

    cats = cats.unstack().reset_index().drop('level_0', axis=1)
    return (cats[cats[0] >= min_cat_cases]['mapped_category_id'])


def get_category_list_df(categories_table, min_cases_feature_creation):
    categories = categories_table.dropna()

    categories['mapped_category_id'] = categories['mapped_category_id'].astype(int)
    # categories['category_id'] = categories['category_id'].apply(lambda x:x[:-2])

    use_cats = get_use_cats(categories_table, min_cases_feature_creation)

    categories = categories[['product_id', 'mapped_category_id']]

    #tools.getLogInstance().info("all category-product combinations: %d" % categories.shape[0])

    # @TODO: set unused categories to special value
    categories = categories[categories.category_id.isin(use_cats)]
    #tools.getLogInstance().info("used category-product combinations: %d" % categories.shape[0])

    # somewhat inefficient
    cat_group = pd.DataFrame(categories.groupby('product_id')['mapped_category_id'].apply(list)).reset_index()

    return cat_group
