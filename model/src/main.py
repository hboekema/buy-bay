""" Main program file """

import pandas as pd
import numpy as np

from sklearn import preprocessing
from sklearn.feature_extraction import text
from sklearn_pandas import DataFrameMapper

import preprocessing as preproc_helpers
import csv_loader as loader
import format
import model
import evaluate

""" Global constants """

# Stop words to be used in mapper
stop_word_list = ['product', 'aan', 'bij', 'die', 'er', 'is', 'te', 'de', 'met', 'in', 'het', 'een', 'en', 'dit',
                  'op']

global_mapper = DataFrameMapper([
        ('days_from_first', None),
        #('df_1', None),
        #('df_2', None),
        ('df_3', preproc_helpers.NaFiller(replacement=0)),
        #('df_4', None),
        #('ddf_1', None),
        #('ddf_2', None),
        #('ddf_3', None),
        #('avg_RoC', None),
        #('days_from_prev_1', None),
        #('days_from_prev_2', None),
        #('days_from_prev_3', None),
        #('days_from_prev_4', None),
        #('days_from_prev_5', None),
        #('price_perc_prev_1', None),
        ('price_perc_prev_2', preproc_helpers.NaFiller(replacement=0)),
        #('price_perc_prev_3', None),
        #('price_perc_prev_4', None),
        #('price_perc_prev_5', None),
        ('price_class', None),
        #('days_from_date', None),
        #('days_from_christmas', None),
        ('weekday', None),
        #('week', None),
        ('month', [preprocessing.LabelBinarizer(sparse_output=True)]),
        #('season', [preprocessing.LabelBinarizer(sparse_output=True)]),
        ('year', [preprocessing.LabelBinarizer(sparse_output=True)]),
        #('product_id', [preprocessing.LabelBinarizer(sparse_output=True)]),
        #('brand_id', [preprocessing.LabelBinarizer(sparse_output=True)]),
        ('description_nl', [text.TfidfVectorizer(max_features=150, stop_words=stop_word_list)]),
        #('mapped_category_id', [preproc_helpers.WithUnseenMultiLabelBinarizer(sparse_output=True)])
        ], sparse=True, df_out=False, default=False)

""" Main functions """


def store_single_step_pred(data=None, load_dirs=None, save_dirs=None):
    """ Builds model and saves predictions and scores over a single prediction step. Param 'data' is a list of data to
    be used, otherwise param 'load_dirs' should be set. Params 'load_dirs' and 'save_dirs' should be lists of (5) load
    and (2) save directories, respectively. """
    # Check if user is being sensible
    if data is None and load_dirs is None:
        print('\nNo data given. Exiting program.')
        return 1

    elif data is not None and load_dirs is not None:
        print('\nParams \'data\' and \'load_dir\' cannot both be used. Using default (\'data\').')
        load_dirs = None

    if load_dirs is not None:
        # Use load directories
        test_df = loader.load_csv(load_dirs[4], format_df=False)

    elif data is not None:
        # Load data from list
        test_df = data[4]

    # Build model
    m, dtrain, dtest = model.generate_model(data, load_dirs, save_dirs[2])

    # Predict
    preds = pd.Series(model.predict(m, dtest))

    results = pd.concat([test_df, preds], axis=1)
    results = results.rename(columns={0: 'predictions'})

    if save_dirs is not None:
        if save_dirs[0] is not None:
            results.to_csv(save_dirs[0])

    # Also format data such that it can be easily plotted
    if not isinstance(results.price_perc, float):
        results.price_perc = results.price_perc.astype(float)

    results = results.dropna(axis=0, subset=['days_from_first'])
    if not isinstance(results.days_from_first.dtype, int):
        try:
            results.days_from_first = results.days_from_first.astype(int)
        except ValueError:
            results.days_from_first = results.days_from_first.astype(float)
            results.days_from_first = results.days_from_first.astype(int)

    mean_price_perc = results.groupby('days_from_first')['price_perc'].mean()
    mean_preds = results.groupby('days_from_first')['predictions'].mean()
    grouped_data = pd.DataFrame(pd.concat([mean_price_perc, mean_preds], axis=1)).sort_index()

    if save_dirs is not None:
        if save_dirs[1] is not None:
            grouped_data.to_csv(save_dirs[1])

    return results, grouped_data


def gather_data(split_type='id', save=True):
    # Load data
    categories_df = loader.load_csv('../data/category_mappings.csv', format_df=False)
    data_df = loader.load_csv('../data/xgb_data.csv')

    # Filter categories from 'mapped_category_id' that do not appear at least a certain number of times
    categories_df = format.filter_categories(categories_df, min_cases=1)

    # Merge the mapped category ids of products to their internal category id
    categories_df = pd.DataFrame(categories_df.groupby(['category_id'])['mapped_category_id'].apply(list)).reset_index()
    merged_df = pd.merge(data_df, categories_df, how='left', left_on='internal_category_id', right_on='category_id')

    # Drop unnecessary columns and format DataFrame
    drop_list = ['service_orderline_price_changes_id', 'service_orderline_id', 'ean', 'article_code',
                 'internal_category_id']
    merged_df = merged_df.drop(labels=drop_list, axis=1)

    # Change by pandas unrecognised None characters in 'stock_new_price' to python's None
    merged_df.loc[:, 'stock_new_price'] = merged_df.stock_new_price.apply(lambda x: None if x == r'\\N' or x == r'\N' else x)

    # This step is crucial - the mapping will not work without this
    merged_df.loc[:, 'mapped_category_id'] = merged_df['mapped_category_id'].apply(lambda d: d if isinstance(d, list)
    else [])

    # Create training sets
    training_set, test_set = format.create_training_test_set(merged_df, split_type=split_type, silent=False)

    if save:
        test_set.to_csv('../data/test_dfs/test_df.csv')
        training_set.to_csv('../data/train_dfs/train_df.csv')

    return training_set, test_set


def map_data(train_data, test_data, mapper=None, save=True):
    """ Maps data. """
    # Specify which columns to keep and what operations to perform on them
    if mapper is None:
        mapper = global_mapper

    # Perform mappings on the training data
    train_X, train_y, test_X, test_y, train_names, test_names = model.perform_mapping(mapper, train_data, test_data)

    if save:
        # Save all this data
        loader.save_sparse_csr('../data/train/training_data', train_X)
        loader.save_sparse_csr('../data/test/test_data', test_X)
        np.save('../data/train/training_labels', train_y)
        np.save('../data/test/test_labels', test_y)

        np.save('../data/names/train_names', train_names)
        np.save('../data/names/test_names', test_names)

    return train_X, train_y, test_X, test_y, test_data, mapper, train_names, test_names


def run_model(reformat=False):
    """ Run ML model """
    load_dirs = ['../data/train/training_data', '../data/train/training_labels.npy', '../data/test/test_data',
                 '../data/test/test_labels.npy', '../data/test_dfs/test_df.csv', '../data/train_dfs/train_df.csv',
                 '../data/names/train_names', '../data/names/test_names']
    save_dirs = ['../data/preds/predictions.csv', '../data/preds/plotting.csv', '../models/step.model',
                 '../data/preds/forecast.csv', '../data/eval/eval.csv', '../data/predictions_test.csv']

    #train_set, test_set = gather_data(split_type='time_id')
    train_set = loader.load_csv(load_dirs[5], format_df=False, infer_dtype=True, type_conversion=True)
    test_set = loader.load_csv(load_dirs[4], format_df=False, infer_dtype=True, type_conversion=True)

    data = list(map_data(train_set, test_set, mapper=global_mapper))

    m, dtrain, dtest = model.generate_model(train_names=data[6], test_names=data[7], load_dir=load_dirs[0:4],
                                            save_dir=save_dirs[2])

    pdf, errors = evaluate.eval_forecast(data[4], model=m, mapper=data[5], periods=None, num_entries=5, method='step',
                                         custom_set=False, density=0, watchlist=['rmse', 'mae'], reformat=reformat,
                                         start=0, save_dirs=[save_dirs[4], save_dirs[5]])
    print(errors)


def test():
    ls = np.linspace(0, 3, 4)
    print(ls)
    df = pd.DataFrame(1, index=ls, columns=['hi'])
    print(df)


if __name__ == "__main__":
    # evaluate.eval_features(iterations=500, watchlist=['rmse', 'mae'])
    # evaluate.eval_errors()
    run_model(reformat=True)
    # test()
