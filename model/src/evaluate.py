""" File containing functions to evaluate the data set """


import datetime
from itertools import compress
import re
import ast

import pandas as pd
import numpy as np

from sklearn import preprocessing
from sklearn.feature_extraction import text
from sklearn_pandas import DataFrameMapper

import format
import model as src_model
import csv_loader as loader
import preprocessing as preproc_helpers


def count_time_gap_occurrences(df, gap_size=30):
    """ Counts the number of times a gap of greater than or equal to gap_size in the days_from_first column occurs. """
    df = df[['product_id'] + ['created_at']].copy()

    # Ensure that days_from_first was calculated relative to first entry
    df = format.days_from_first(df)

    # Sort by product_id, then by days_from_first
    df = df.sort_values(by=['product_id', 'days_from_first'], ascending=[True, True])
    df['gap'] = df['days_from_first'].diff()

    # A cunning construction to overcome the difficulty of negative gaps when there is a change in product_id
    df.loc[df['gap'] < 0, 'gap'] = 0

    df = df[df['gap'] >= gap_size]
    count_per_id = pd.DataFrame(pd.pivot_table(df, index='product_id', values='gap', aggfunc='count'))
    count_per_id = count_per_id.unstack().reset_index().drop('level_0', axis=1)
    count_per_id.columns = ['product_id', 'gap_occurrence']

    # Calculated the percentage of products that have a time gap of at least the specified size
    perc_dropped = len(count_per_id) / len(df) * 100

    # Merge DataFrames to allow the mean gap occurrences to be calculated
    new_df = pd.merge(df, count_per_id, on='product_id', how='left').fillna(0).drop_duplicates(subset='product_id')
    mean = new_df['gap_occurrence'].mean()

    return {'mean': round(mean, 2), 'percentage of total': round(perc_dropped, 1)}


def count_multiple_price_occurrences(df):
    """ Counts the number of times a product has multiple price changes on the same day. """
    # Ensure that created_at is rounded to days
    df = df[['product_id'] + ['created_at'] + ['ref_new_price']]
    length_df = len(df)

    if not isinstance(df.created_at.dtype, datetime.datetime):
        df['created_at'] = pd.to_datetime(df['created_at'])

    try:
        df['created_at'] = df['created_at'].dt.date
    except TypeError:
        df = df

    pivot_df = pd.DataFrame(pd.pivot_table(df, index=['product_id', 'created_at'], values='ref_new_price',
                                           aggfunc='count'))

    pivot_df = pivot_df[pivot_df['ref_new_price'] > 1]
    length = len(pivot_df)

    perc_products = round(len(pivot_df.index.levels[0]) / len(df.drop_duplicates('product_id')) * 100)

    return {'number of cases': length, 'percentage of total (rows)': round(length / length_df * 100, 1),
            'percentage of total (products)': perc_products}


def extract_multiple_price_data(df, aggfunc):
    """ Function capable of finding the minimum, maximum and average price change for prices that were found on the
     same day - WIP. """
    df = df[['product_id'] + ['created_at'] + ['ref_new_price']]

    if not isinstance(df.ref_new_price.dtype, float):
        df['ref_new_price'] = df['ref_new_price'].astype(float)

    if not isinstance(df.created_at.dtype, datetime.datetime):
        df['created_at'] = pd.to_datetime(df['created_at'])

    try:
        df['created_at'] = df['created_at'].dt.date
    except TypeError:
        df = df

    if isinstance(aggfunc, list):
        aggfunc.append('count')
        aggfunc = tuple(aggfunc)
    else:
        aggfunc = tuple(['count', aggfunc])

    df = df.drop_duplicates(subset=['product_id', 'created_at', 'ref_new_price'])

    pivot_df = pd.DataFrame(pd.pivot_table(df, index=['product_id', 'created_at'], values='ref_new_price',
                                           aggfunc=aggfunc))

    # @TODO: Finish function
    # Filter out any dates with only one price change
    pivot_df = pivot_df[pivot_df['count'] > 1]
    print(pivot_df)


""" Functions that test the XGBoost model forecasting performance """


def eval_forecast(df, model, mapper, periods, num_entries, method='step', start=0, custom_set=True, density=None,
                  watchlist=None, reformat=False, save_dirs=None):
    """ Evaluates the forecast() function in src.model. """
    if periods is not None and not 0 <= start < periods:
        raise ValueError("'start' must lie between 0 and 'periods'")

    # Create a custom subset of the data
    if custom_set and periods is not None:
        if density is not None:
            test_set = format.min_density_set(df, days=periods, density=density)
        else:
            test_set = format.first_n_days_set(df, num_days=periods)
    else:
        test_set = df

    # Only feed in the data of the 'start'-th entry for each product id
    test_set = format.add_prior_entries(test_set, add=True)
    first_entry = test_set[test_set['prior_entries'] == start].reset_index(drop=True).copy()

    if periods is not None:
        fcast = src_model.forecast(first_entry, model, mapper, periods-start, num_entries, method)
    else:
        # Infer how far to predict for each product
        forecast_range = format.infer_periods(test_set, start=start)
        fcast = src_model.forecast(first_entry, model, mapper, forecast_range, num_entries, method)

    fcast = fcast.sort_values(['product_id', 'days_from_first'], ascending=[True, True]).reset_index(drop=True)
    test_set = test_set.sort_values(['product_id', 'days_from_first'], ascending=[True, True]).reset_index(drop=True)

    if reformat:
        fcast = format.reformat_preds(fcast, test_set)
        test_set = format.change_start_pos(test_set)

    grouped_fcast = fcast.groupby('days_from_first')['predictions'].mean()
    grouped_df = test_set.groupby('days_from_first')['price_perc'].mean()

    # Put actual prices and predictions side-by-side
    performance_df = pd.concat([grouped_df, grouped_fcast], axis=1)
    performance_df = performance_df.rename(columns={'price_perc': 'actual'})

    if save_dirs is not None:
        try:
            fcast.to_csv(save_dirs[0])
            performance_df.to_csv(save_dirs[1])
        except IndexError:
            print('Save failed.')

    errors = []

    if watchlist is not None:
        # Calculate the errors in the predictions
        for measure in watchlist:
            if measure == 'rmse':
                rmse = calculate_rmse(performance_df)
                errors.append(rmse)
            elif measure == 'mae':
                mae = calculate_mae(performance_df)
                errors.append(mae)

    return performance_df, errors


def calculate_mae(df):
    """ Calculates the mean-absolute-error of predictions. """
    return (abs(df['predictions'] - df['actual'])).mean()


def calculate_rmse(df):
    """ Calculates the root-mean-square error of predictions. """
    return np.sqrt((df['predictions'] - df['actual']).pow(2).mean())


""" Models that facilitate the evaluation of the impact of combinations of features on the model """


def eval_features(iterations, watchlist=None):
    """ Evaluates the effect of adding or removing features on a model's performance. """
    train_set, test_set = eval_prep(split_type='time_id')

    for iteration in range(iterations):
        print("\nEvaluating model {}...".format(iteration + 1))
        features = create_feature_list(iteration=(iteration, iterations/2))
        mapper = DataFrameMapper(features, sparse=True, df_out=False, default=False)
        data = list(src_model.perform_mapping(mapper, train_set, test_set))

        model, dtrain, dtest = src_model.generate_model(data=data[:4], train_names=data[4], test_names=data[5])

        perf_df, errors = eval_forecast(test_set, model, mapper, periods=None, num_entries=5, start=0, custom_set=False,
                                        density=None, watchlist=watchlist, save_dirs=None)

        # Write errors to the log
        error_str = ''
        for i in range(len(errors)):
            error_str += ' | {}: {}'.format(watchlist[i], errors[i])

        with open('../logs/log4.txt', mode='a') as log:
            log.write('\nFeatures: {} \n{}\n'.format(str([item[0] for item in features]), error_str))


def eval_prep(split_type='id'):
    """ Prepares just the training and test sets (for model evaluation). """
    # Load data
    categories_df = loader.load_csv('../data/category_mappings.csv', format_df=False)
    data_df = loader.load_csv('../data/xgb_data.csv')

    # Filter categories from 'mapped_category_id' that do not appear at least a certain number of times
    categories_df = format.filter_categories(categories_df, min_cases=5)

    # Merge the mapped category ids of products to their internal category id
    categories_df = pd.DataFrame(categories_df.groupby(['category_id'])['mapped_category_id'].apply(list)).reset_index()
    merged_df = pd.merge(data_df, categories_df, how='left', left_on='internal_category_id', right_on='category_id')

    # Drop unnecessary columns and format DataFrame
    drop_list = ['service_orderline_price_changes_id', 'service_orderline_id', 'stock_new_price', 'ean', 'article_code',
                 'internal_category_id']

    # This step is crucial - the mapping will not work without this
    merged_df.loc[:, 'mapped_category_id'] = merged_df['mapped_category_id'].apply(lambda d: d if isinstance(d, list)
    else [])
    merged_df = merged_df.drop(labels=drop_list, axis=1)

    # Create training sets
    training_set, test_set = format.create_training_test_set(merged_df, split_type=split_type, silent=False)

    # Save these data sets
    training_set.to_csv('../data/train_dfs/eval_train.csv')
    test_set.to_csv('../data/test_dfs/eval_test.csv')

    return training_set, test_set


def read_errors():
    """ Reads features and errors in log. """
    features = []
    rmse = []
    mae = []
    depths = []

    depth = 0

    with open('../logs/log4.txt', mode='r') as log:
        for line in log:
            if 'Depth' in line:
                depth = int(re.findall("\d+", line)[0])

            elif 'Features' in line:
                line = ast.literal_eval(line[10:-2])
                features.append(tuple(line))
                depths.append(depth)

            elif 'rmse' and 'mae' in line:
                floats = re.findall("\d+\.\d+", line)
                rmse.append(float(floats[0]))
                mae.append(float(floats[1]))

            elif 'rmse' in line:
                floats = re.findall("\d+\.\d+", line)
                rmse.append(float(floats))

            elif 'mae' in line:
                floats = re.findall("\d+\.\d+", line)
                mae.append(float(floats))

    features_series = pd.Series(features)
    depth_series = pd.Series(depths)
    rmse_series = pd.Series(rmse)
    mae_series = pd.Series(mae)

    feat_df = pd.concat([features_series, depth_series, rmse_series, mae_series], axis=1)
    feat_df.columns = ['features', 'depth', 'rmse', 'mae']

    mapper = DataFrameMapper([
        ('features', [preproc_helpers.WithUnseenMultiLabelBinarizer(sparse_output=True)]),
        ('depth', None),
        ('rmse', None),
        ('mae', None)
    ], sparse=False, df_out=True, default=False)

    mapped_feats = mapper.fit_transform(feat_df)
    mapped_feats.to_csv('../data/eval/feature_errors.csv')

    return mapped_feats


def eval_errors(mapped_feats=None):
    """ Evaluates features and their contribution to the model. """
    if mapped_feats is None:
        mapped_feats = read_errors()

    col_eval_list = []

    for column in mapped_feats.columns:
        if column != 'rmse' and column != 'mae':
            temp = mapped_feats.groupby(column)['rmse', 'mae'].mean()
            temp.loc[:, 'name'] = temp.index.name
            col_eval_list.append(temp)

    cols = mapped_feats.drop(['depth', 'rmse', 'mae'], axis=1).columns
    length = len(cols)
    col_pairs = []
    col_pairs_eval = []

    for i in range(length):
        for j in range(i + 1, length):
            # Saves all unique column pairs
            col_pairs.append([cols[i], cols[j]])

    for col_pair in col_pairs:
        temp = mapped_feats.groupby(col_pair)['rmse', 'mae'].mean()
        index_names = temp.index.names
        temp.loc[:, 'name_1'] = index_names[0]
        temp.loc[:, 'name_2'] = index_names[1]
        col_pairs_eval.append(temp)

    error_df = pd.concat(col_eval_list, axis=0).reset_index().set_index(['name', 'index'])
    error_df.to_csv('../data/eval/individual_feat_eval.csv')

    pair_error_df = pd.concat(col_pairs_eval, axis=0).reset_index()
    pair_error_df.columns = ['index_1', 'index_2', 'rmse', 'mae', 'name_1', 'name_2']
    pair_error_df = pair_error_df.set_index(['name_1', 'index_1', 'name_2', 'index_2'])
    pair_error_df.to_csv('../data/eval/pair_feat_eval.csv')

    return error_df, pair_error_df


""" Functions for randomly selecting features"""


def create_bool_list(total_features, perc=0.5):
    """ Creates list of randomly generate bool values. """
    bool_list = np.random.rand(total_features) > perc
    if bool_list.any():
        return bool_list
    else:
        if perc < 1:
            return create_bool_list(total_features, perc)
        else:
            return create_bool_list(total_features, perc-0.1)


def create_feature_list(iteration=None):
    """ Creates list of features """
    # Stop words to be used in mapper
    stop_word_list = ['product', 'aan', 'bij', 'die', 'er', 'is', 'te', 'de', 'met', 'in', 'het', 'een', 'en', 'dit',
                      'op']

    features = [
        ('days_from_first', None),
        ('df_1', None),
        ('df_2', None),
        ('df_3', None),
        ('df_4', None),
        ('ddf_1', None),
        ('ddf_2', None),
        ('ddf_3', None),
        ('avg_RoC', None),
        ('days_from_prev_1', None),
        ('days_from_prev_2', None),
        ('days_from_prev_3', None),
        ('days_from_prev_4', None),
        ('days_from_prev_5', None),
        ('price_perc_prev_1', None),
        ('price_perc_prev_2', None),
        ('price_perc_prev_3', None),
        ('price_perc_prev_4', None),
        ('price_perc_prev_5', None),
        ('price_class', None),
        ('days_from_date', None),
        ('days_from_christmas', None),
        ('weekday', None),
        ('week', None),
        ('month', [preprocessing.LabelBinarizer(sparse_output=True)]),
        ('season', [preprocessing.LabelBinarizer(sparse_output=True)]),
        ('year', [preprocessing.LabelBinarizer(sparse_output=True)]),
        ('product_id', [preprocessing.LabelBinarizer(sparse_output=True)]),
        ('brand_id', [preprocessing.LabelBinarizer(sparse_output=True)]),
        ('description_nl', [text.TfidfVectorizer(max_features=150, stop_words=stop_word_list)]),
        ('mapped_category_id', [preproc_helpers.WithUnseenMultiLabelBinarizer(sparse_output=True)])
        ]

    if iteration is not None:
        perc = weight_number(iteration[0], mean=iteration[1], invert=True)
    else:
        perc = 0.5

    bool_list = create_bool_list(len(features), perc)
    return list(compress(features, bool_list))


def weight_number(num, mean, invert=False):
    """ Uses custom weighting scheme to weight numbers. Normalised relative to mean. """
    try:
        norm_num = (num / mean) - 1
    except ZeroDivisionError:
        print("\nMean cannot be 0 for this function.")
        raise

    if invert:
        norm_num = -norm_num

    weighted = (pow(norm_num, 3) + 1) / 2
    if weighted < 0.1:
        weighted += 0.01
    elif weighted > 0.9:
        weighted -= 0.01

    return weighted
