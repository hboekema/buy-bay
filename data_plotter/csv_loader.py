""" Loads a CSV file and converts it into a pandas DataFrame """

import pandas as pd


def load_csv(path, type='SOPC', format=True):
    """ Loads data from a CSV file into a pandas DataFrame with the option to format it appropriately (recommended)
    :param path: The path to the CSV file in the user's directory
    :param type: The type of file to be analysed. This is only applicable if format is True. Options are 'SOPC' \
    (Service Orderline Price Changes) and 'PPC' (Product Price Changes)
    :param format: Whether or not to format the file
    :return: pandas DataFrame
    """

    if format is False: return pd.read_csv(path)
    else:
        DF = pd.read_csv(path)

        if type == 'SOPC':
            DF.columns = ['Avg. Price', 'Min. Price', 'Max. Price', 'Price Range', 'Price Range as % of Avg. Price',
                          '# Price Changes', '# Price Changes (Diff. Days)', '% Price Change Over Time',
                          'Range of Time Data', 'Days Since Last Price Change', 'Data Density']
        elif type == 'PPC':
            DF.columns = ['Product ID', 'Avg. Price', 'Min. Price', 'Max. Price', 'Price Range', '# Prices',
                          '# Prices (Diff. Channels)', '# Price Changes (Diff. Days)', 'Days on Market']
        else:
            print('Type not recognised.')
        return DF
