""" The main program file """

import csv_loader as loader
import plot_data


def main():
    path = 'data/analysed_data.csv'
    data = loader.load_csv(path, type='SOPC')
    print('Data collected.')
    """values = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 250, 300, 350, 400]
    filtered_data1 = plot_data.data_filter(data, values, 'dates_range')
    plot_data.plot_DF(filtered_data1, title='Range of available date data')"""
    values = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 250, 300, 350, 400]
    filtered_data2 = plot_data.data_filter(data, values, 'num_prices')
    plot_data.plot_DF(filtered_data2, title='Number of prices per product')
    """filtered_data2 = plot_data.data_filter(data, values, 'price_change')
    plot_data.plot_DF(filtered_data2, title='Price drop')"""


if __name__ == "__main__":
    main()