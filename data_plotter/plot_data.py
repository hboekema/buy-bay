""" File containing functions that filter and plot data. """

import matplotlib.pyplot as plt
import pandas as pd


def plot_DF(dataframe, kind='bar', title='Graph'):
    """ Plots a DataFrame object
    :param dataframe: the pandas DataFrame to be plotted
    :param kind: The kind of plot to show. Options are the same as for pd.DataFrame.plot()
    :return: Shows a graph but does not return anything
    """
    dataframe.plot(kind=kind, title=title)
    plt.show()


def data_filter(dataframe, categories, filter):
    """ Filters data out of a pandas DataFrame
    :param dataframe: The pandas DataFrame to be filtered
    :param categories: The values/categories to filter data by - should be a list or np array
    :param filter: The type of filter to be applied - choice of 'num_prices', 'dates_range' and 'price_change'
    :return: pandas DataFrame with data grouped according to filter
    """
    # The following code has the same function as a switch statement
    choices = {'num_prices': '# Price Changes (Diff. Days)', 'dates_range': 'Range of Time Data', 'price_change':
               '% Price Change Over Time'}
    result = choices.get(filter)

    if result is None:
        print('\nThis filter type is unknown. All data returned.')
        return dataframe

    # Create 'folders' for the different values/categories
    folder = dict()
    for category in categories:
        folder[category] = 0

    # Get the entries to be filtered from the DataFrame
    entries = dataframe[result]

    # Filter the entries, only permitting the to be in a single category
    for entry in entries:
        for category in reversed(categories):
            if entry >= category:
                folder[category] += 1
                break

    return dict_to_dataframe(folder)


def dict_to_dataframe(dict):
    """ Converts dict to DataFrame using pandas to_dict and formats the result """
    dataframe = pd.DataFrame.from_dict(data=dict, orient='index')
    dataframe.sort_index(inplace=True)
    dataframe.index.name = 'Category'
    dataframe.columns = ['Number of Products']
    return dataframe
