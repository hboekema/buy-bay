""" Will fall under 'train_future_prices.py' """


import argparse

parser = argparse.ArgumentParser("train")
parser.add_argument("--environment", help="set to pick environment, e.g. production|development", type=str,
                    required=True)
parser.add_argument("--allow_model_upload", help="set to allow allow model upload to cloud storage", type=str,
                    required=False)

args = parser.parse_args()
environment = args.environment
allow_model_upload = args.allow_model_upload

# coding: utf-8

try:

    import sys
    import os

    import numpy as np
    import pandas as pd

    pd.options.mode.chained_assignment = None  # to prevent chained assignment warnings that are not relevant

    import math
    import scipy as sp

    # Required for db writing and reading
    import sqlite3
    from pandas.io import sql  # and reading (opt)

    import logging
    import time
    import datetime

    # Data science pipeline tools
    from sklearn.feature_extraction import text
    from sklearn.model_selection import train_test_split
    from sklearn.utils import shuffle
    from sklearn.datasets import dump_svmlight_file, load_svmlight_file
    from sklearn import preprocessing

    from sklearn.externals import joblib

    # To make pipeline work with pandas column input
    from sklearn_pandas import DataFrameMapper

    # Custom pipeline tools
    import helpers.tools as tools_helpers
    import helpers.preprocessing as preproc_helpers
    import helpers.extraction as extraction_helpers
    import helpers.settings
    import private_settings

    # Grab settings
    params = helpers.settings.parameters[environment]
    split_train_test = params['split_train_test']
    train_model_directly = params['train_model_directly']
    push_model_to_gcloud = params['push_model_to_gcloud']
    use_sqlite = params['use_sqlite']

    if push_model_to_gcloud and not allow_model_upload:
        raise Exception("push_model_to_gcloud is set but allow_model_upload not")

    # Work with whole percentages
    discount_resolution = 1

    # Select the number of previous entries to add data from to each data point
    num_entries = 3

    metadata_path = "./training/input_data/"
    export_path = "./training/output_data/"
    db_path = "./training/input_data/"

    pd.set_option("display.max_columns", 80)

    run_type = "future_prices"
    run = extraction_helpers.TrainingRunParameters(run_type=run_type, export_path=export_path)

    # Enable logging such that executed processes can be tracked
    import logging

    tools_helpers.initLogger(run_type, logging.INFO, logging.INFO)
    logging = tools_helpers.getLogInstance()

    # Log start of run
    logging.info("Parameters: " + str(params))
    logging.info("start run %s" % run.run_id)


    def timeit(method):

        def timed(*args, **kw):
            ts = time.time()
            result = method(*args, **kw)
            te = time.time()

            print('%r %2.2f sec' % (method.__name__, te - ts))
            return result

        return timed


    if use_sqlite:
        logging.info("development environment; use sqlite db")
        import sqlite3

        con = sqlite3.connect(db_path + '/bb_export_april.db')
    else:
        import pymysql
        import pymysql.cursors

        db_settings = private_settings.database

        con = pymysql.connect(host=db_settings['host'],
                              ssl=db_settings['ssl'],
                              user=db_settings['user'],
                              password=db_settings['password'],
                              db=db_settings['db'],
                              charset='utf8mb4',
                              cursorclass=pymysql.cursors.DictCursor)

    @timeit
    def get_pc_data(con):
        # Fetch only the data needed for to train this model
        query = """ SELECT * FROM view_training_price_predictions """
        stock = sql.read_sql(query, con)

        return stock

    pc = get_pc_data(con)

    def prepare_future_prices(df, entries_ago=5):
        """ Applies filters and adds features for optimal XGBoost performance. """
        # Set columns to the correct types
        df = preproc_helpers.created_at_to_dt(df)
        df = preproc_helpers.id_to_int(df)
        df = preproc_helpers.price_to_float(df)

        # Filter duplicates, keeping only the lowest price for each product
        df = df.sort_values(by=['products_id', 'created_at', 'ref_new_price'], ascending=[True, True, True])
        df = df.drop_duplicates(subset=['products_id', 'created_at'], keep='first').reset_index(drop=True)

        # Add column 'first_price' to the DataFrame
        df = extraction_helpers.find_first_price(df)

        # Add column 'days_from_first' to the DataFrame
        df = extraction_helpers.days_from_first(df)

        # Add column 'price_perc' to the DataFrame
        df = extraction_helpers.add_price_perc(df)

        # Apply filters
        df = preproc_helpers.filter_by_price_range(df, max_range=0.5)
        df = preproc_helpers.filter_by_num_prices(df, min_cases=30)
        df = preproc_helpers.rm_gaps(df, gap_size=80)

        # Convert 'description_nl' to type str
        df = preproc_helpers.description_to_str(df)

        # Add column 'weekday' to the DataFrame
        df = extraction_helpers.add_weekday(df)

        # Add column 'months' to the DataFrame
        df = extraction_helpers.add_month(df, as_string=True)

        # Add column 'year' to the DataFrame
        df = extraction_helpers.add_year(df)

        # Add ordinal categorical variable grouping prices into classes to DataFrame
        df = extraction_helpers.add_price_class(df, start=0, stop=500, step=50)

        # Change time format to be compatible with the model and prediction method
        df = preproc_helpers.change_time_format(df, format_type='step', entries_ago=entries_ago, rate_of_change=True)

        return df

    # Format data and add useful features to it
    df = prepare_future_prices(pc, num_entries)

    stop_word_list = ['product', 'aan', 'bij', 'die', 'er', 'is', 'te', 'de', 'met', 'in', 'het', 'een', 'en', 'dit',
                      'op']

    # Note: supply single items
    # Note: supply DataTypeSaver first to be able to map in api
    mapper = DataFrameMapper([
        # Item related fields - add the DataTypeSaver for fields that are exposed in the API
        ('days_from_first', [preproc_helpers.DataTypeSaver()]),
        ('df_3', [preproc_helpers.DataTypeSaver()]),
        ('price_perc_prev_2', [preproc_helpers.DataTypeSaver()]),
        ('price_class', [preproc_helpers.DataTypeSaver()]),
        ('weekday', [preproc_helpers.DataTypeSaver(), preprocessing.LabelBinarizer(sparse_output=True)]),
        ('month', [preproc_helpers.DataTypeSaver()], preprocessing.LabelBinarizer(sparse_output=True)),
        ('year', [preproc_helpers.DataTypeSaver()], preprocessing.LabelBinarizer(sparse_output=True)),
        ('products_description_nl', [preproc_helpers.DataTypeSaver(), preproc_helpers.NaFiller(replacement=''),
                                     text.TfidfVectorizer(max_features=150, stop_words=stop_word_list)]),

    ], sparse=True, df_out=False, default=False)


    @timeit
    def persist_traintest_data(data_X, data_y, traintest_type="train"):
        # Store train/test data
        svm_path = run.get_export_path("new" + traintest_type + ".svm")
        dump_svmlight_file(data_X, data_y, svm_path, zero_based=True, multilabel=False)

    # Split data into sets or don't depending on the user's intentions
    if split_train_test:
        train, test = preproc_helpers.create_training_test_set(df, split_type='time_id', silent=False)
        train_X, train_y, test_X, test_y, train_names, test_names = preproc_helpers.perform_mapping(mapper, train, test)
    else:
        train_X, train_y, train_names = preproc_helpers.perform_mapping(mapper, df, train_only=True)

    # Optionally train model directly
    if train_model_directly:
        import xgboost as xgb

        # Prepare DMatrix
        dtrain = xgb.DMatrix(train_X, label=train_y, feature_names=train_names)
        param = helpers.settings.xgb_prices_training_params

        logging.info("Model training with params: " + str(param))

        # Use pre-defined settings
        n_rounds = helpers.settings.xgb_prices_n_rounds
        bst = xgb.train(params=param, dtrain=dtrain, num_boost_round=n_rounds)
        bst.save_model(run.get_export_path('xgb.model'))

        # Also save params
        run.params['training'] = {}
        run.params['training']['params'] = param
        run.params['training']['boost_rounds'] = n_rounds
        run.params['training']['duration'] = run.get_elapsed_time()

    if not train_model_directly:
        # Store data instead
        persist_traintest_data(train_X, train_y, "train")

        if split_train_test:
            persist_traintest_data(test_X, test_y, "test")

    joblib.dump(mapper, run.get_export_path('preprocessing_mapper.joblib'))

    run.params["train_rows"] = train_X.shape[0]
    run.params["use_external_weights_file"] = None
    run.params["split_train_test"] = split_train_test

    run.params["transformed_columns"] = extraction_helpers.get_transformed_columns(mapper, df)
    used_columns = list(extraction_helpers.get_features(mapper).keys())
    run.params["sample_input_values_json"] = df[used_columns].sample(1).to_json(orient='records')
    run.params["environment"] = environment

    run.dump_parameters()

    # Log end of run
    logging.info("Finished run in %.0f seconds ", run.get_elapsed_time())

    # Push models to cloud
    runid = ''
    if push_model_to_gcloud:

        import helpers.storage

        if 'use_test_storage_location' in params and params['use_test_storage_location']:
            helpers.storage.bucket_name = helpers.storage.test_bucket_name

        bucket = helpers.storage.get_bucket()
        runid = run.run_id

        uploaded = helpers.storage.upload_model_data(runid, run_type, bucket, export_path)
        logging.info("Uploaded %d files to storage bucket %s" % (uploaded, helpers.storage.bucket_name))

    return_var = runid

except Exception as exc:
    if 'logging' in globals():
        logging.exception("Exception during preprocessing and training")
        logging.exception(exc)
    else:
        print("***Exception but no logger:" + str(exc))

    return_var = ''

finally:
    # do any cleanup required here...

    print(return_var)
    pass
