""" Will fall under 'preprocessing.py' """


import datetime
import time

import pandas as pd
import numpy as np

from .extraction import add_price_perc, days_from_first, days_from_prev


""" Model-related operations """


def perform_mapping(mapper, training_set=None, test_set=None, train_only=False, test_only=False, silent=False):
    """ Maps dummy variables. """
    if train_only and test_only:
        raise ValueError("Only one of 'train_only' and 'test_only' can be True.")

    if not silent:
        print('\nStarting mappings...')
    start = time.time()

    if not test_only:
        train_X = mapper.fit_transform(training_set)
        train_y = training_set.price_perc.values
        train_names = mapper.transformed_names_

    if not train_only:
        test_X = mapper.transform(test_set)
        test_y = test_set.price_perc.values
        test_names = mapper.transformed_names_

    if not silent:
        print('Mappings executed in {} seconds'.format(round((time.time() - start), 2)))

    if train_only:
        return train_X, train_y, train_names
    if test_only:
        return test_X, test_y, test_names

    return train_X, train_y, test_X, test_y, train_names, test_names


""" Functions that adjust the format of the DataFrame entries """


def change_time_format(df, format_type='default', entries_ago=1, rate_of_change=True):
    """ Changes the format of the time-related data of the DataFrame to a program-specified type.

    Params:
    ------
    df: pandas DataFrame
    format_type: the style desired for the data. Choices are:
    - 'default': the default formatting style. 'days_from_first' is measured relative to the first date and 'ref_new_price' is
                 the price pertaining to the current day.
    - 'step': 'days_from_first' indicates the days since the previous price point; 'ref_new_price' is the price pertaining to
              the current day. A new column 'price_perc_prev' will be added to the DataFrame containing the most recent
              previous price percentage.
    - 'first': perhaps the most controversial option. 'days_from_first' is taken in relation to the first date. However,
               column 'price_perc_initial' will be added to the DataFrame - this will always be 1.
    entries_ago: only to be used if format_type is 'step'. Indicates the number of previous prices to add to the data.
    rate_of_change: only to be used if format_type is 'step'. Calculates the rate of change between the past
                    'entries_ago' entries.

    Returns:
    -------
    pandas DataFrame

    """

    if format_type == 'default':
        df = days_from_first(df)
        df = add_price_perc(df)

    elif format_type == 'step':
        df = df.sort_values(by=['products_id', 'created_at'], ascending=[True, True]).reset_index(drop=True)

        try:
            df['price_perc']
        except KeyError:
            # Add 'price_perc'
            df = add_price_perc(df)

        # Track when the last price changes occurred
        df = days_from_prev(df, overwrite=False, entries_ago=entries_ago)

        # Find the last few prices
        for entry in reversed(range(entries_ago)):
            df.loc[:, 'price_perc_prev_{}'.format(entry + 1)] = df['price_perc'].shift(entry + 1)

            # Set 'price_perc_prev' for the first 'entries_ago' entries of each product to None
            grouped_df = df.groupby('products_id').head(entry + 1)
            mask = df.index.isin(grouped_df.index)
            df.loc[mask, 'price_perc_prev_{}'.format(entry + 1)] = None

        if rate_of_change is True:
            for entry in reversed(range(entries_ago)):
                if entry > 0:
                    # Calculate the average rate of change of the past few prices
                    df.loc[:, 'df_{}'.format(entry)] = (df.loc[:, 'price_perc_prev_{}'.format(entry)] -
                                                                    df.loc[:, 'price_perc_prev_{}'.format(entry+1)]) / \
                                                                   (df.loc[:, 'days_from_prev_{}'.format(entry)])

            for entry in reversed(range(entries_ago-1)):
                if entry > 0:
                    # Calculate the average rate of change of the average rate of change of the past few prices
                    df.loc[:, 'ddf_{}'.format(entry)] = 2 * ((df.loc[:, 'df_{}'.format(entry)] -
                                                        df.loc[:, 'df_{}'.format(entry+1)]) /
                                                       (df.loc[:, 'days_from_prev_{}'.format(entry+1)] +
                                                        df.loc[:, 'days_from_prev_{}'.format(entry+2)]))

            # Calculate the average rate of change since the first price point
            df.loc[:, 'avg_RoC'] = (1 - df['price_perc_prev_1']) / df['days_from_first']

    elif format_type == 'first':
        df = days_from_first(df)
        df.loc[:, 'price_perc_initial'] = 1.0

    else:
        print('Format not recognised. Original DataFrame returned.')

    return df


""" Functions that handle time gaps in product entries """


def rm_gaps(df, gap_size=5):
    """ Removes gaps of at least size 'size' (per product id), keeping the longest stretch of data without
    such gaps. NOTE: Also drops duplicates of 'products_id' and 'days_from_first' """
    df = id_to_int(df)
    df = price_to_float(df)

    # Ensure that days_from_first was calculated
    try:
        df['days_from_first']
    except KeyError:
        df = days_from_first(df)

    df = df.sort_values(['products_id', 'days_from_first', 'ref_new_price'], ascending=[True, True, True])
    temp = df[['products_id', 'created_at', 'days_from_first']].copy()
    temp = temp.drop_duplicates(subset=['products_id', 'days_from_first'])

    # Sort by products_id, then by days_from_first
    temp = temp.sort_values(by=['products_id', 'days_from_first'], ascending=[True, True]).reset_index(drop=True)
    temp.loc[:, 'gap'] = temp['days_from_first'].diff()

    # A cunning construction to overcome the difficulty of negative gaps when there is a change in products_id
    temp.loc[(temp['gap'] < 0), 'gap'] = 0

    # Guard the location of gaps that exceed the limit
    temp.loc[:, 'mask'] = temp['gap'] >= gap_size

    # Set the mask for the first entry of each products_id to True
    first_occurrence = temp.drop_duplicates(subset=['products_id'], keep='first')
    mask = temp.index.isin(first_occurrence.index)
    temp.loc[mask, 'mask'] = True

    # Do the same for the last entry
    last_occurrence = temp.drop_duplicates(subset=['products_id'], keep='last')
    mask = temp.index.isin(last_occurrence.index)
    temp.loc[mask, 'mask'] = True

    # Count the number of data points between the last two gaps
    trues = temp['mask'].values == 1
    falses = ~trues

    counts = np.cumsum(falses)[trues]
    counts[1:] -= counts[:-1].copy()

    gap_counts = temp[trues]
    gap_counts.loc[:, 'count'] = counts

    # Necessary to preserve the index (which is needed in a later step)
    gap_counts.loc[:, 'i'] = gap_counts.index

    # Remove products with only one data point here
    occurrence_table = pd.pivot_table(gap_counts, index=['products_id'], values=['days_from_first'], aggfunc=len)
    occurrence_table = occurrence_table.unstack().reset_index().drop('level_0', axis=1)
    occurrence_table = occurrence_table[occurrence_table[0] >= 2]

    i1 = gap_counts.set_index('products_id').index
    i2 = occurrence_table.set_index('products_id').index

    gap_counts = gap_counts[i1.isin(i2)]

    # Find the longest stretch of data points and the dates these lie between
    max_counts = gap_counts.sort_values('count', ascending=False).groupby('products_id', as_index=False).first()
    max_counts = max_counts[max_counts['count'] > 0]  # Throw away entries with a count of 0

    # Format DataFrame
    gap_counts = gap_counts.sort_values(by=['products_id', 'days_from_first'], ascending=[True, True])

    prior_to_max = gap_counts['i'].isin(max_counts['i'])
    prior_to_max = prior_to_max.shift(-1).fillna(False)
    dates = pd.concat([gap_counts[prior_to_max], max_counts], axis=0).sort_values(by='products_id')

    # Store the indices of the two 'important' values per product id in a list of tuples
    indices = dates.sort_values(by='i', ascending=True).groupby('products_id')['i'].apply(tuple).reset_index()['i'] \
        .values

    del dates  # no longer useful

    concat_list = []

    # Now throw out any entries that do not fall between the aforementioned indices
    for index_tup in indices:
        concat_list.append(temp.iloc[index_tup[0]:index_tup[1]])

    # Create the final DataFrame
    no_gaps = pd.concat(concat_list, axis=0)

    # Create final DataFrame
    mask2 = df.index.isin(no_gaps.index)
    df = df[mask2].reset_index(drop=True)

    # Reset the 'days_from_first' and 'price_perc' columns
    try:
        # Drop column (if present)
        df = df.drop('first_price', axis=1)
    except ValueError:
        df = df

    df = add_price_perc(df)

    return df


""" Functions that filter the data on some criterion/criteria """


def filter_categories(categories_df, min_cases):
    """ Filters out categories that do not appear at least min_cases times. """
    categories_table = pd.pivot_table(categories_df, index='mapped_category_id', aggfunc=len)
    categories_table = categories_table.unstack().reset_index().drop('level_0', axis=1)
    mask = categories_df.index.isin(categories_table[categories_table[0] >= min_cases].index)

    return categories_df[mask]


def filter_by_num_prices(df, min_cases):
    """ Filters out products that do not have the requisite number of cases ('min_cases'). """
    # Remove products with fewer than a certain number of price entries
    price_table = pd.pivot_table(df, index=['products_id'], values=['ref_new_price'], aggfunc=len)
    price_table = price_table.unstack().reset_index().drop('level_0', axis=1)
    price_table = price_table[price_table[0] >= min_cases]

    # Only keep products in df that are also in (the filtered) price_table
    i1 = df.set_index('products_id').index
    i2 = price_table.set_index('products_id').index

    return df[i1.isin(i2)].reset_index(drop=True)


def filter_by_price_change(df, max_change=50):
    """ Calculates price change between two consecutive price entries for all price entries, per products_id. Then it
    filters out the last of two consecutive rows that have a greater than 'max_change' (a percentage) price change
    between them. Uses recursion and is as such VERY slow for large DataFrames (under stringent conditions). """
    # Prepare DataFrame by converting types etc.
    df = price_to_float(df)
    df = created_at_to_dt(df)

    initial_length = len(df)

    # Sort by products_id, then sort the prices chronologically
    temp_df = df.sort_values(by=['products_id', 'created_at'], ascending=[True, True]).copy()

    temp_df.loc[:, 'price_difference'] = temp_df['ref_new_price'].diff().fillna(0)

    # However, this causes difficulties when there is a change in products_id. Thus, filter out duplicates, keeping just
    # the first entry per DataFrame. Then, set 'price_difference' to 0.
    no_duplicates = temp_df.drop_duplicates(subset='products_id', keep='first')
    index = no_duplicates.index.tolist()

    temp_df.loc[index, 'price_difference'] = 0

    # Shift the price differences up a row
    temp_df.loc[:, 'price_difference'] = temp_df['price_difference'].shift(-1).fillna(0)

    # Now the percentage change (relative to the prior price) can be calculated
    temp_df.loc[:, 'perc_price_diff'] = temp_df['price_difference'] / temp_df['ref_new_price'] * 100

    # Shift back and filter out rows with price changes greater than 'max_change'
    temp_df.loc[:, 'perc_price_diff'] = temp_df['perc_price_diff'].shift(1).fillna(0)
    temp_df = temp_df[temp_df['perc_price_diff'] <= max_change]

    # Drop columns added by this function
    temp_df = temp_df.drop(labels=['price_difference', 'perc_price_diff'], axis=1)

    if len(temp_df) != initial_length:
        temp_df = filter_by_price_change(temp_df, max_change)

    return temp_df


def filter_by_price_range(df, max_range=0.4):
    """ Finds the range of prices of products in a DataFrame and filters out products with a range of more than
    'max_range' percent (as decimal) of the initial price. """
    df = id_to_int(df)

    try:
        df['price_perc']
    except KeyError:
        df = add_price_perc(df)

    # Find the range
    max = (df.groupby('products_id').max())['price_perc']
    min = (df.groupby('products_id').min())['price_perc']
    range_df = (max - min)

    # Perform the check and return all the entries that pass the condition
    check = range_df[range_df <= max_range]
    mask = df.products_id.isin(check.index)
    return df[mask].reset_index(drop=True)


""" Functions that create data subsets on specific criteria """


def create_training_test_set(df, split_type='id', silent=True):
    """ Create training and test set of type chosen by user. 'split_type' options are 'id', 'time', 'time_id' and
    (default) 'random'. """
    if split_type == 'id':
        # Ensure that all entries of each product are in the same set
        training_set, test_set = create_id_subset(df, size=0.8)

    elif split_type == 'time':
        # Split the training and test data by 'created_at'
        training_set, test_set = create_time_subset(df, days=100)

    elif split_type == 'time_id':
        # Split by the recency of entries, by product id
        training_set, test_set = create_time_id_subset(df, size=0.8)

    else:
        # Simply create random samples
        training_set = df.sample(frac=0.8, replace=False)

        mask = df.index.isin(training_set.index)
        test_set = df[~mask]

    if not silent:
        print("Training set length: {}".format(len(training_set)))
        print("Test set length: {}\n".format(len(test_set)))

    return training_set, test_set


def create_id_subset(df, size=0.8):
    """ Create subset of given DataFrame, splitting on 'products_id'. Size is a decimal of total DataFrame length """
    if not 0 <= size <= 1:
        raise ValueError("Parameter 'size' must be a float between 0.0 and 1.0 inclusive")

    # Sort DataFrame on products_id
    df = id_to_int(df)

    prod_ids = pd.Series(df['products_id'].unique())
    id_sample = prod_ids.sample(frac=0.8)
    print(id_sample)

    mask = df.products_id.isin(id_sample.values)
    set_1 = df[mask].reset_index(drop=True)
    set_2 = df[~mask].reset_index(drop=True)

    return set_1, set_2


def create_time_subset(df, days=30):
    """ Creates subsets of given DataFrame, splitting on 'created_at'. Days is the point at which to make the split. """
    if days < 0:
        raise ValueError("Parameter 'days' must be a positive integer")

    # Ensure 'created_at' is the right format
    df = created_at_to_dt(df)

    newest = df.sort_values(by='created_at', ascending=False)['created_at'].values[0]

    # Filter out entries with a 'days_from_first' of greater than 'days'
    mask = df['created_at'] >= (newest - datetime.timedelta(days))
    newest = df[mask]
    oldest = df[~mask]  # The other data subset

    return oldest.reset_index(drop=True), newest.reset_index(drop=True)


def create_time_id_subset(df, size=0.8):
    """ Creates a subset of the first 'size' data points (as decimal of total) per product id. """
    if not 0 <= size <= 1:
        raise ValueError("Parameter 'size' must be a float between 0.0 and 1.0 inclusive")

    df = created_at_to_dt(df, date_only=True)
    df = id_to_int(df)

    try:
        df['days_from_first']
    except KeyError:
        df = days_from_first(df)

    try:
        df['price_perc']
    except KeyError:
        df = add_price_perc(df)

    # Duplicates must be dropped for this function to work properly
    df = df.sort_values(by=['products_id', 'created_at', 'price_perc'], ascending=[True, True, True])
    df = df.drop_duplicates(subset=['products_id', 'created_at'], keep='first').reset_index(drop=True)

    # Find the most recent entries and create a 'boundary' entry
    newest_entries = df.groupby('products_id')['days_from_first'].max().reset_index()
    newest_entries = newest_entries.rename(columns={'days_from_first': 'newest_entry'})
    temp = df.merge(newest_entries, on='products_id', how='left')
    temp.loc[:, 'boundary_entry'] = (temp['newest_entry'] * size).astype(int)

    # Make the split
    mask = temp['days_from_first'] <= temp['boundary_entry']
    oldest = temp[mask]
    newest = temp[~mask]

    oldest = oldest.drop(labels=['newest_entry', 'boundary_entry'], axis=1)
    newest = newest.drop(labels=['newest_entry', 'boundary_entry'], axis=1)

    return oldest.reset_index(drop=True), newest.reset_index(drop=True)


""" Format types """


def convert_feat_types(df, date_only=True):
    """ Applies all of the below conversions. WARNING: does not add features of itself - if a feature is absent an
    exception will be raised. """
    df = created_at_to_dt(df, date_only=date_only)
    df = id_to_int(df)
    df = brand_id_to_str(df)
    df = price_to_float(df)
    df = days_to_int(df)
    df = description_to_str(df)
    df = cat_ids_to_str(df)
    df = year_to_str(df)

    return df


def created_at_to_dt(df, date_only=True):
    """ Converts 'created_at' column in DataFrame to type 'datetime'. Optionally only returns the date. """
    if not isinstance(df.created_at.dtype, datetime.datetime):
        # May raise an exception - this is deliberately left unhandled
        df.loc[:, 'created_at'] = pd.to_datetime(df.created_at)
    if date_only and not isinstance(df.created_at.dtype, datetime.date):
        # Returns type 'object'
        df.loc[:, 'created_at'] = pd.to_datetime(df.created_at)
        df.loc[:, 'created_at'] = df.created_at.dt.date

    return df


def year_to_str(df):
    if not isinstance(df.year.dtype, int):
        df.loc[:, 'year'] = df.year.astype(int)

    return df


def id_to_int(df):
    """ Converts 'products_id' column in DataFrame to type 'int'. """
    if not isinstance(df.products_id.dtype, int):
        # May raise an exception - this is deliberately left unhandled
        df.loc[:, 'products_id'] = df.products_id.astype(int)

    return df


def brand_id_to_str(df):
    """ Converts 'brand' column in DataFrame to type 'str'. """
    if not isinstance(df.brand_id.dtype, str):
        # May raise an exception - this is deliberately left unhandled
        df.loc[:, 'brand_id'] = df.brand_id.astype(str)

    return df


def price_to_float(df):
    """ Converts 'ref_new_price' (and potentially also 'price_perc') column in DataFrame to type 'float'. """
    if not isinstance(df.ref_new_price.dtype, float):
        # May raise an exception - this is deliberately left unhandled
        df.loc[:, 'ref_new_price'] = df.ref_new_price.astype(float)
    try:
        if not isinstance(df.price_perc.dtype, float):
            df.loc[:, 'price_perc'] = df.price_perc.astype(float)
    except (KeyError, AttributeError):
        df = df
    return df


def days_to_int(df):
    """ Converts 'days_from_first' column in DataFrame to type 'int'. """
    if not isinstance(df.days_from_first.dtype, int):
        # May raise an exception - this is deliberately left unhandled
        df.loc[:, 'days_from_first'] = df.days_from_first.astype(int)

    return df


def description_to_str(df):
    """ Converts 'products_description_nl' column in DataFrame to type 'str'. """
    if not isinstance(df.products_description_nl.dtype, str):
        # May raise an exception - this is deliberately left unhandled
        df.loc[:, 'products_description_nl'] = df.products_description_nl.astype(str)

    return df


def cat_ids_to_str(df):
    """ Converts entries in 'mapped_category_id' column in DataFrame to type 'str'. """
    df.loc[:, 'mapped_category_id'] = df.mapped_category_id.apply(lambda x: (str(entry) for entry in x))

    return df


def entry_to_array(entry, dtype=str):
    """ Converts entries to an array with types of 'dtype'. """
    return np.asarray(entry, dtype=dtype)
