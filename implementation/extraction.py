""" Will fall under 'extraction.py' """


import itertools
import datetime
import pandas as pd

from .preprocessing import id_to_int, price_to_float, created_at_to_dt


""" Global constants """

holidays = [('christmas', datetime.datetime(2017, 12, 25))]
months_of_year = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',
                  'November', 'December']

""" Functions that add a column to the DataFrame """


def add_prior_entries(df, add=True):
    """ Adds the number of entries prior to each entry, grouped by 'product_id'. """
    df = id_to_int(df)
    df = price_to_float(df)
    df = created_at_to_dt(df)

    # Duplicates must be dropped for this function to work properly
    df = df.sort_values(by=['product_id', 'created_at', 'ref_new_price'], ascending=[True, True, True])
    df = df.drop_duplicates(subset=['product_id', 'created_at']).reset_index(drop=True)

    counts = df.groupby('product_id')['created_at'].nunique().values

    # Create and flatten lists - this is fairly inefficient
    counts = pd.Series(list(itertools.chain(*list(list(range(x)) for x in counts))))

    if add:
        df.loc[:, 'prior_entries'] = counts
        return df
    else:
        return counts


def add_price_perc(df, relative_to_first=True, transform=None, drop_price=False):
    """ Adds column with price as a percentage of the first price to the DataFrame. Param 'transform' allows user to
    apply a transformation function on this column. """
    if relative_to_first:
        try:
            df['first_price']
        except KeyError:
            df = find_first_price(df)

        df.loc[:, 'price_perc'] = df['ref_new_price'] / df['first_price']
        if drop_price:
            df = df.drop('first_price', axis=1)

    else:
        try:
            df['last_price']
        except KeyError:
            df = find_last_price(df)

        df.loc[:, 'price_perc'] = df['ref_new_price'] / df['last_price']
        if drop_price:
            df = df.drop('last_price', axis=1)

    if transform is not None:
        df.loc[:, 'price_perc'] = df.loc[:, 'price_perc'].apply(transform)

    return df


def add_weekday(df):
    """ Adds a column that indicates which day of the week the entry in the DataFrame was created. """
    df = created_at_to_dt(df, date_only=False)
    df.loc[:, 'weekday'] = df['created_at'].dt.weekday
    return df


def add_week(df):
    """ Adds a column with the week of the year in which the DataFrame entry was created. """
    df = created_at_to_dt(df, date_only=False)
    df.loc[:, 'week'] = df['created_at'].dt.week
    return df


def add_month(df, as_string=False):
    """ Adds a column with the month of the year in which the DataFrame entry was created. """
    df = created_at_to_dt(df, date_only=False)
    df.loc[:, 'month'] = df['created_at'].dt.month

    if as_string:
        for i in range(1, len(months_of_year) + 1):
            df.loc[df['month'] == i, 'month'] = months_of_year[i - 1]

    return df


def add_season(df):
    """ Adds seasons as strings. """
    try:
        df['month']
    except KeyError:
        df = add_month(df, as_string=True)

    months = months_of_year
    month = df['month']

    # Classify data
    winter = (month == 12) | (month == 1) | (month == 2) | \
             (month == months[11]) | (month == months[0]) | (month == months[1])
    spring = (month == 3) | (month == 4) | (month == 5) | \
             (month == months[2]) | (month == months[3]) | (month == months[4])
    summer = (month == 6) | (month == 7) | (month == 8) | \
             (month == months[5]) | (month == months[6]) | (month == months[7])
    autumn = (month == 9) | (month == 10) | (month == 11) | \
             (month == months[8]) | (month == months[9]) | (month == months[10])

    # Set categories
    df.loc[winter, 'season'] = 'winter'
    df.loc[spring, 'season'] = 'spring'
    df.loc[summer, 'season'] = 'summer'
    df.loc[autumn, 'season'] = 'autumn'

    return df


def add_year(df):
    """ Adds a column that indicates which year the entry in the DataFrame was created. """
    df = created_at_to_dt(df, date_only=False)
    df.loc[:, 'year'] = df['created_at'].dt.year
    return df


def add_days_from_date(df, date):
    """ Adds a column that indicates the difference between a date and the date the entry in the DataFrame was
    created. """
    df = created_at_to_dt(df, date_only=False)
    df.loc[:, 'days_from_date'] = (df['created_at']-date).dt.days
    return df


def add_days_from_holidays(df, holidays):
    """ Add columns containing the date to a holiday. Holidays name and dates should be provided through the
    'holidays' parameter in the form of a list of (name, date) tuples. """
    if not (isinstance(holidays, list) or isinstance(holidays, tuple)):
        raise TypeError('Incorrect parameter format')
    if not (all((isinstance(holiday, list) or isinstance(holiday, tuple)) for holiday in holidays)):
        raise TypeError('Incorrect parameter format')

    df = created_at_to_dt(df, date_only=False)

    for name, date in holidays:
        df.loc[:, 'days_from_{}'.format(name)] = (df['created_at'] - date).dt.days % -365
    return df


def days_from_first(df):
    """ Adds a column containing the number of days since the first entry of a product to the DataFrame """
    df = created_at_to_dt(df)
    df = df.reset_index(drop=True)

    no_duplicates = df.sort_values(by='created_at', ascending=True).drop_duplicates(subset='product_id', keep='first')

    first_price = no_duplicates[['product_id'] + ['created_at']]
    first_price.columns = ['product_id', 'first_date']
    merged_df = pd.merge(df, first_price, on='product_id', how='inner')

    df.loc[:, 'days_from_first'] = merged_df['created_at'] - merged_df['first_date']
    df.loc[:, 'days_from_first'] = df['days_from_first'].dt.days

    df = df.dropna(subset=['days_from_first']).reset_index(drop=True)
    df.loc[:, 'days_from_first'] = df.days_from_first.astype(int)

    return df


def days_from_last(df):
    """ Adds a column containing the number of days 'since' the last entry of a product to the DataFrame """
    df = created_at_to_dt(df)
    df = df.reset_index(drop=True)

    no_duplicates = df.sort_values(by='created_at', ascending=False).drop_duplicates(subset='product_id', keep='first')

    last_price = no_duplicates[['product_id'] + ['created_at']]
    last_price.columns = ['product_id', 'last_date']
    merged_df = pd.merge(df, last_price, on='product_id', how='inner')

    df.loc[:, 'days_from_last'] = merged_df['created_at'] - merged_df['last_date']
    df.loc[:, 'days_from_last'] = df['days_from_last'].dt.days

    df = df.dropna(subset=['days_from_last']).reset_index(drop=True)
    df.loc[:, 'days_from_last'] = df.days_from_first.astype(int)

    return df


def days_from_prev(df, overwrite=False, entries_ago=1, from_last=False):
    """ Adds a column containing the number of days since the previous entry of a product to the DataFrame. This
    function will overwrite the 'days_from_first' column if it already exists and parameter 'overwrite' is True. Param
    'entries_ago' can only be used if 'overwrite' is False. This parameter allows the user to calculate the days since
    from the last 'entries_ago' entries. 'from_last' allows this function to be used with function days_from_last(). """
    df = created_at_to_dt(df)
    df = id_to_int(df)

    try:
        df['days_from_first']
    except KeyError:
        df = days_from_first(df)

    df = df.sort_values(by=['product_id', 'created_at'], ascending=[True, True]).reset_index(drop=True)

    if overwrite:
        # Calculate days since last price point and format
        df.loc[:, 'days_from_first'] = df['created_at'].diff().dt.days.fillna(0).astype(int)

        # Overcomes difficulties that arise when there is a change in product id
        df.loc[df['days_from_first'] < 0, 'days_from_first'] = 0

        if from_last:
            df.loc[:, 'days_from_first'] = -df.loc[:, 'days_from_first']

    else:
        for entry in reversed(range(entries_ago)):
            # Calculate days since last price point and format
            df.loc[:, 'days_from_prev_{}'.format(entry + 1)] = df['created_at'].diff().dt.days.fillna(0).astype(int) \
                .shift(entry)

            # Overcomes difficulties that arise when there is a change in product id
            df.loc[df['days_from_prev_{}'.format(entry + 1)] < 0, 'days_from_prev_{}'.format(entry + 1)] = None

            if from_last:
                df.loc[:, 'days_from_prev_{}'.format(entry + 1)] = -df.loc[:, 'days_from_prev_{}'.format(entry + 1)]

    return df


def find_first_price(df):
    """ Finds the first (and lowest if multiple prices on day 0) price of each product and adds it to the DataFrame. """
    df = price_to_float(df)
    df = created_at_to_dt(df)

    no_duplicates = df.sort_values(by=['product_id', 'created_at', 'ref_new_price'], ascending=[True, True, True]) \
        .drop_duplicates(subset='product_id', keep='first')

    first_price = no_duplicates[['product_id'] + ['ref_new_price']]
    first_price.columns = ['product_id', 'first_price']

    try:
        # Allows column 'first_price' to be overwritten
        df = df.drop('first_price', axis=1)
    except ValueError:
        df = df

    return pd.merge(df, first_price, on='product_id').reset_index(drop=True)


def find_last_price(df):
    """ Finds the last (lowest if multiple prices on last day) price of each product and adds this to the DataFrame. """
    df = price_to_float(df)
    df = created_at_to_dt(df)

    no_duplicates = df.sort_values(by=['product_id', 'created_at', 'ref_new_price'], ascending=[True, False, True]) \
        .drop_duplicates(subset='product_id', keep='first')

    last_price = no_duplicates[['product_id'] + ['ref_new_price']]
    last_price.columns = ['product_id', 'last_price']

    return pd.merge(df, last_price, on='product_id').reset_index(drop=True)


def add_price_class(df, start=0, stop=250, step=50):
    """ Makes the 'first_price' of each entry into an ordinal categorical variable and adds this to the DataFrame.
    The parameters represent the DataFrame, start price, stop price and range size, respectively. """
    # Prepare data
    df = price_to_float(df)

    try:
        df['first_price']
    except KeyError:
        df = find_first_price(df)

    price = df['first_price']
    ordinal_num = 0
    price_class_list = []

    for price_low_bound in range(start, stop, step):
        # Assign a class to the entries with prices falling between each range
        temp = df[(price > price_low_bound) & (price < price_low_bound + step)]
        if len(temp) > 0:
            temp.loc[:, 'price_class'] = ordinal_num
            price_class_list.append(temp)
        ordinal_num += 1

    # Deal with products with prices beyond 'stop'
    expensive_products = df[price > stop]
    if len(expensive_products) > 0:
        expensive_products.loc[:, 'price_class'] = ordinal_num
        price_class_list.append(expensive_products)

    return pd.concat(price_class_list, axis=0).reset_index(drop=True)
