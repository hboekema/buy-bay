""" Will fall under 'predictions.py' """


import numpy as np

from extraction import add_prior_entries


""" Helper functions for forecasting """


def infer_periods(df, start=0):
    """ Infers how many periods to predict prices for (for each product in the DataFrame). Returns a list of ints. """
    try:
        df['days_from_first']
    except KeyError:
        df = add_prior_entries(df, add=True)

    # Find number of entries for each product
    grouped_df = df.groupby('products_id')['days_from_first']
    periods = grouped_df.max().values - grouped_df.min().values
    forecast_range = np.asarray(periods - start)
    forecast_range[forecast_range < 0] = 0  # For safety

    return forecast_range



""" Functions that reformat the data for aesthetic purposes """


def change_start_pos(df):
    """ Makes first entry per product occur on day 0 with a 'price_perc' of 1.0 """
    df = days_from_first(df)
    df = find_first_price(df)
    df = add_price_perc(df)

    return df


def reformat_preds(preds, test_set=None):
    """ Makes first entry per product occur on day 0 with 'predictions' scaled relative to the first price entry (in
     'preds') for each product. """
    if test_set is None:
        # Much more inaccurate scaling than if a test_set is provided!!
        preds = days_from_first(preds)
        preds.loc[:, 'first_price_old'] = preds['first_price']
        preds = find_first_price(preds)
        preds = add_price_perc(preds)

        factor = preds['first_price'] / preds['first_price_old']
        preds = preds.drop('first_price_old', axis=1)

        try:
            # Scale predictions
            preds.loc[:, 'predictions'] = preds['predictions'] * factor
        except KeyError:
            print('\nPredictions must have been made in order for this function to work.')
            raise
    else:
        # Test set must have been predicted on, otherwise this function will not work as expected
        test_set_copy = test_set.copy()
        test_first_price = test_set_copy.sort_values(['products_id', 'days_from_first'], ascending=[True, True])\
            .drop_duplicates('products_id')
        test_first_price = test_first_price[['products_id', 'price_perc']]
        test_first_price.columns = ['products_id', 'first_perc']

        preds = preds.merge(test_first_price, on='products_id')

        try:
            # Scale predictions
            preds.loc[:, 'predictions'] = preds['predictions'] / preds['first_perc']
        except KeyError:
            print('\nPredictions must have been made in order for this function to work.')
            raise

        # Drop unwanted/inaccurate columns
        preds = preds.drop(['first_perc', 'price_perc'], axis=1)
        preds = days_from_first(preds)

    return preds
