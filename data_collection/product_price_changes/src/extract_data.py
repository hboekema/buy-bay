""" This file contains functions used to extract data from panda dataframes and arrays of Product objects """


from datetime import datetime
import pandas as pd
import csv_loader as loader
import product_data


""" The following functions use pandas functions to retrieve data from a DataFrame. Note: a dictionary is returned """

def mean_price_by_product_id(dataframe, to_dict=True):
    sorted_dataframe = dataframe.groupby('product_id').mean()
    if to_dict: return sorted_dataframe['price'].to_dict()
    else: return sorted_dataframe['price']


def max_price_by_product_id(dataframe, to_dict=True):
    sorted_dataframe = dataframe.groupby('product_id').max()
    if to_dict: return sorted_dataframe['price'].to_dict()
    else: return sorted_dataframe['price']


def min_price_by_product_id(dataframe, to_dict=True):
    sorted_dataframe = dataframe.groupby('product_id').min()
    if to_dict: return sorted_dataframe['price'].to_dict()
    else: return sorted_dataframe['price']


def first_date(dataframe, to_dict=True):
    sorted_dataframe = dataframe.groupby('product_id').min()
    if to_dict: return sorted_dataframe['created_at'].to_dict()
    else: return sorted_dataframe['created_at']


def last_date(dataframe, to_dict=True):
    sorted_dataframe = dataframe.groupby('product_id').max()
    if to_dict: return sorted_dataframe['created_at'].to_dict()
    else: return sorted_dataframe['created_at']

""" The following functions do not use pandas DataFrame functions """


def range_available_time_data(dataframe):
    """ Finds the range of time data for each product_id. """
    first = first_date(dataframe)
    last = last_date(dataframe)
    range = dict()

    product_ids = dataframe['product_id']

    for product_id in product_ids:
        try:
            # Calculate the time, in days, between the first data point and the last data point 
            range[product_id] = (loader.str_to_dt(last[product_id]).date() -
                                 loader.str_to_dt(first[product_id]).date()).days
        except ValueError:
            # Ignore this data point 
            range[product_id] = None

    return range


def num_price_changes(dataframe, total_changes=True, diff_days=True, diff_channels=True):
    """ Returns the number of price changes that have occurred since the creation of this product """
    change_dict = dict()
    time_dict = dict()
    sales_dict = dict()

    if total_changes:
        product_ids = dataframe['product_id']
        for product_id in product_ids:
            if product_id not in change_dict:
                change_dict[product_id] = 0

            change_dict[product_id] += 1    # Increment this every time a product is found

    if diff_days:
        length = len(dataframe)
        days_dict = dict()

        cols = ['product_id'] + ['created_at']
        df = dataframe[cols]

        for i in range(length):
            # Prepare for the test conditions (below)
            row = df.ix[i]
            product_id = row['product_id']
            created = loader.str_to_dt(row['created_at'])

            if product_id not in days_dict:
                days_dict[product_id] = []

            if product_id not in time_dict:
                time_dict[product_id] = 0

            # The following code determines whether or not the prices for this product should be included
            skip = False

            day_created = created.date()
            if day_created in days_dict[product_id]:
                skip = True

            if skip is False:
                days_dict[product_id].append(day_created)   # Add this date to the days_dict
                time_dict[product_id] += 1

    if diff_channels:
        length = len(dataframe)
        channel_dict = dict()

        cols = ['product_id'] + ['sales_channel_id']
        df = dataframe[cols]

        for i in range(length):
            row = df.ix[i]
            product_id = row['product_id']
            channel_id = row['sales_channel_id']

            if product_id not in channel_dict:
                channel_dict[product_id] = []

            if product_id not in sales_dict:
                sales_dict[product_id] = 0

            # The following code determines whether or not the prices for this product should be included
            skip = False

            if channel_id in channel_dict[product_id]:
                skip = True

            if skip is False:
                channel_dict[product_id].append(channel_id)  # Add this channel to the channel_dict
                sales_dict[product_id] += 1

    return_list = []

    if total_changes is True: return_list.append(change_dict)
    else: return_list.append(None)
    if diff_days is True: return_list.append(time_dict)
    else: return_list.append(None)
    if diff_channels is True: return_list.append(sales_dict)
    else: return_list.append(None)

    return return_list


def price_over_time(dataframe):
    """ Finds and stores the price of a product over time """
    price_dict = dict()

    cols = ['product_id'] + ['price'] + ['created_at']      # Only consider the useful data

    df = dataframe[cols]
    length = len(df.index)

    for i in range(length):
        # Get the product_id, ref_new_price and created_at data out of this row
        row = df.ix[i]
        product_id = row['product_id']
        created = row['created_at']
        ref_price = row['price']

        if product_id not in price_dict:
            price_dict[product_id] = []

        price_dict[product_id].append((created, ref_price))  # Add a (date, price) tuple to list

    return price_dict


def price_change_over_time(dataframe, price_data=None):
    """ Returns the change in price (as a percentage of the original price) between the first and last price in the
    DataFrame. The parameter price_data, if provided, allows the program to skip the calculation of prices over
    time. """
    if price_data is None: price_data = price_over_time(dataframe)    # Significantly extends runtime

    price_change = dict()

    product_ids = dataframe['product_id']

    for product_id in product_ids:
        # Just get first and last price entry from the price_data (see price_over_time() for more info)
        first = price_data[product_id][0][1]    # This retrieves the second entry (price) of the first entry (first data
        #  point) of the value associated with product_id
        last = price_data[product_id][-1][1]    # This retrieves the second entry (price) of the last entry (last data
        #  point) of the value associated with product_id

        price_change[product_id] = 100 * (last - first) / first

    return price_change


def since_product_price_change(dataframe):
    """ Returns the last time the product prices were updated (in days) """
    time_dict = dict()

    cols = ['product_id'] + ['created_at']
    df = dataframe[cols]
    length = len(df)

    for i in range(length):
        row = df.ix[i]
        product_id = row['product_id']
        product_creation = time_since_creation(row)

        if product_id not in time_dict:
            time_dict[product_id] = product_creation

        else:
            # Only consider the most recent price change
            most_recent = time_dict[product_id]

            if product_creation < most_recent:
                time_dict[product_id] = product_creation

    return time_dict


def avg_prices_per_product(products):
    """ Finds the average number of prices per product of a list of Product objects"""
    value_sum = 0
    key_sum = 0

    products = loader.dataframe_to_series(products)

    if type(products) is not dict:
        if type(products[0]) is product_data.Product:   # Conversion to row form necessary
            products = loader.objects_to_rows(products)

        products = num_prices_per_product(products)

    for key in products:
        key_sum += 1
        value_sum += products[key]

    return value_sum / key_sum


""" Misc. functions """


def time_since_creation(datum, days_only=True):
    """ Finds the time a certain product has spent on the market - argument datum should be a row of a dataframe """
    now = datetime.now()
    try:
        created_string = datum['created_at']
        created = loader.str_to_dt(created_string)
        timedelta = now - created
    except ValueError:
        return None

    if days_only:
        return timedelta.days
    else:
        return timedelta


""" Not used much """


def avg_time_per_product(products):
    """ Returns the average number of days a product spends on the market """
    if type(products) is pd.DataFrame: products = loader.dataframe_to_series(products)
    if type(products[0]) is not product_data.Product: products = loader.rows_to_objects(products)

    time_sum = 0
    product_sum = 0

    for product in products:
        time = product.time_on_market()
        if time is not None:
            time_sum += time
        product_sum += 1

    return time_sum / product_sum


def avg_time_on_market(dataframe):
    """ Function that finds the average time spent on market per product and returns this as a dictionary """
    time_dict = dict()      # Tracks the time a product has spent on the market
    count_dict = dict()     # Tracks the number of times that this product has been listed

    length = len(dataframe.columns)

    cols = ['product_id'] + ['created_at']
    df = dataframe[cols]

    for i in range(length):
        row = df.ix[i]
        product_id = row['product_id']
        time = time_since_creation(row)

        if time is not None:
            if product_id not in time_dict:
                time_dict[product_id] = 0
                count_dict[product_id] = 0

            time_dict[product_id] += time
            count_dict[product_id] += 1

    avg_time_dict = dict()  # Use the average instead of total time spent on the market per product

    for product_id in time_dict:
        avg_time_dict[product_id] = time_dict[product_id] / count_dict[product_id]

    return avg_time_dict


def product_count(products, min_condition=4):
    """ Tracks how often a product has been listed - essentially the same as num_prices_per_product """
    if type(products) is pd.DataFrame: products = loader.dataframe_to_series(products)
    if type(products[0]) is not product_data.Product: products = loader.rows_to_objects(products)

    count_dict = dict()
    for product in products:
        product_id = product.product_id
        if product_id not in count_dict:
            count_dict[product_id] = 0

        if product.product_condition_id <= min_condition:
            count_dict[product_id] += 1

    return count_dict
