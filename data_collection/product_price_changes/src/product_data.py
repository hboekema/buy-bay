""" This file contains the product class. This makes it easier to manipulate product data.
Not used much - DataFrame indexing preferred to save time in conversions. """
from datetime import datetime
from dateutil import parser


class Product:
    def __init__(self, product_id, sales_channel_id, country_id, price, seller,
                 created_at):
        self.product_id = product_id
        self.sales_channel_id = sales_channel_id
        self.country_id = country_id
        self.price = price
        self.seller = seller

        try:
            self.created_at = parser.parse(created_at)
        except:
            self.created_at = None

    def time_on_market(self, days_only=True):
        """ Finds the time a certain product has spent on the market """
        now = datetime.now()
        try:
            timedelta = now - self.created_at
        except:
            return None

        if days_only: return timedelta.days
        else: return timedelta
