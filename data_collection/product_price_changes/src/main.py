""" Main program file """


import os
import timeit
from datetime import datetime
import matplotlib.pyplot as plt
import pandas as pd
import csv_loader as loader
import extract_data


def main():
    path = '..//data/PPC_data.csv'
    format_product_data(path, cachedir='../data/stored_data', filename='analysed_PPC.csv', silent=False)


def format_product_data(path, cachedir=None, filename=None, silent=True):
    """ Formats data so that it is easily readable by the user """
    products = loader.load_csv(path)

    start = timeit.default_timer()

    avg_price = extract_data.mean_price_by_product_id(products)
    if not silent: print("\nAverage price done.")
    min_price = extract_data.min_price_by_product_id(products)
    if not silent: print("Minimum price found.")
    max_price = extract_data.max_price_by_product_id(products)
    if not silent: print("Maximum price found.")
    num_price_list = extract_data.num_price_changes(products, total_changes=True, diff_days=True, diff_channels=True)
    num_price = num_price_list[0]
    if not silent: print("Number of prices found")
    num_price_diff_days = num_price_list[1]
    if not silent: print("Number of prices (diff. days) found.")
    num_price_diff_channels = num_price_list[2]
    if not silent: print("Number of prices (diff. channels) found.")
    time_range = extract_data.range_available_time_data(products)
    if not silent: print("Time range of available data calculated.")

    errors = 0      # Tracks how many rows have been omitted
    product_dict = dict()
    products = loader.dataframe_to_series(products)     # Convert so that the product_id can be accessed easily

    for product in products:
        product_id = product[0]     # This gives the price_id without the need to convert to Product objects

        try:
            if product_id not in product_dict:
                avgp = round(avg_price[product_id], 2)
                minp = round(min_price[product_id], 2)
                maxp = round(max_price[product_id], 2)
                pran = round(maxp - minp, 2)    # The price range of this product
                nump = num_price[product_id]
                npdd = num_price_diff_days[product_id]
                npdc = num_price_diff_channels[product_id]
                tmer = time_range[product_id]   # Range of time data for this product

                product_dict[product_id] = [avgp, minp, maxp, pran, nump, npdd, npdc, tmer]
        except:
            errors += 1

    end = timeit.default_timer()
    time_taken = end - start

    print('\nAnalysis performed in {} seconds - {} errors (erroneous data is omitted).\n'.format(time_taken, errors))

    # Convert dict to a DataFrame for easy manipulation
    product_dataframe = pd.DataFrame.from_dict(data=product_dict, orient='index')
    product_dataframe.columns = ['Avg. Price', 'Min. Price', 'Max. Price', 'Price Range', '# Prices',
                                 '# Prices (Diff. Days)', '# Prices (Diff. Channels)', 'Range of Time Data']
    product_dataframe.index.name = 'Product ID'
    product_dataframe.sort_values('Range of Time Data', inplace=True)

    if cachedir is not None:    # Store file in specified directory
        now = datetime.now()
        if filename is None: filename = 'PPC ' + str(now) + '.csv'
        product_dataframe.to_csv(os.path.join(cachedir, filename))
        print('File stored with path {}.'.format(cachedir))

    return product_dataframe


def plot_product_data(dataframe):
    """ Plots a dataframe """
    dataframe.plot()
    plt.show()


if __name__ == "__main__":
    main()
