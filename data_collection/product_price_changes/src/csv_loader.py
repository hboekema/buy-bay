""" Loads data into python from a CSV file and produces an array of rows or Product objects """

from dateutil import parser
import pandas as pd
import numpy as np
import product_data


def load_csv(path, format=True):
    """ Loads data from a CSV file into a pandas DataFrame """
    DF = pd.read_csv(path)
    if format:
        DF.columns = ['product_id', 'sales_channel_id', 'country_id', 'price', 'seller', 'created_at']
    return DF


def str_to_dt(str):
    """ Converts str object to a datetime object """
    try:
        dt = parser.parse(str)
    except ValueError:
        return None

    return dt


""" Not really used """


def stringDF_to_dtDF(dataframe):
    """ Converts DF of str objects to dataframe of float objects """
    values = dataframe.values
    for i in range(len(values)):
        if type(i) is str:
            values[i] = str_to_dt(value)

    return values

def dataframe_to_series(dataframe):
    """ Returns a pandas Series given a pandas DataFrame """
    return dataframe.values


def rows_to_objects(rows):
    """ Returns an array of Product objects given the rows of an array """
    temp = []

    for row in rows:
        temp.append(product_data.Product(row[0], row[1], row[2], row[3], row[4], row[5]))

    return np.array(temp)


def objects_to_rows(products):
    """ Converts an array of Product objects to rows in an array """
    temp = []

    for product in products:
        temp.append([product.product_id, product.sales_channel_id, product.country_id, product.price,
                     product.seller, product.created_at])

    return np.array(temp)
